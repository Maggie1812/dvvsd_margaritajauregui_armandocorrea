`ifndef FIFO
    `define FIFO
import sdp_dc_ram_pkg::*;

module fifo #(
    DATA_WIDTH=W_DATA, 
    BUFFER_DEPTH=W_DEPTH
  )(
    input  logic                    wr_clk,   //clk
    input  logic                    rd_clk,   //clk
    input  logic                    rst,   //rst
    input  logic                    push,  //push enable
    input  logic                    pop,  // pop enable
    input  bit                      flush,
    input  data_t                   DataInput,    //data input
    output data_t                   DataOutput,   //data output
    output logic                    empty,  // signal of empty
    output logic                    full    //signal of full
  );
    
  logic nempty;
  logic nfull;
  logic nempty_next;
  logic nfull_next;
  logic we_next;
  data_t DataOutput_next;
   
  logic [W_ADDR:0] wr_ptr, nwr_ptr;           // Write pointer
   addr_t rd_ptr, nrd_ptr;           // Read pointer

  sdp_dc_ram_if mem_interface();
  
  sdp_dc_ram ram_instance(
    .clk_a(wr_clk),
    .clk_b(rd_clk),
    .mem_if(mem_interface.mem)
  );
 // --------------------------------------------
 // --------------------------------------------
 // --------------READ------------------------------
 // --------------------------------------------
 // --------------------------------------------
  always_ff @(posedge rd_clk or negedge rst) begin 
    if(!rst | flush) begin
         rd_ptr <= '0;
        empty  <= '1;
    end else begin
        empty   <= nempty;
        rd_ptr <= (nrd_ptr != wr_ptr) ? nrd_ptr : rd_ptr;
    end
  end
//----------------POINTERS------------------------

assign nrd_ptr  = (pop && !empty ) ? rd_ptr+1'b1 : rd_ptr;

// ----------------DATA OUT---------------------

  assign DataOutput = DataOutput_next;

  always_ff @(posedge rd_clk or negedge rst) begin
    if(!rst | flush) begin
      mem_interface.rd_addr_b <= 0;
      DataOutput_next <= 'd0 ;
    end else begin //if fifo isnt full data input is saved in the buffer
      mem_interface.rd_addr_b <= (pop && !empty) ? rd_ptr: mem_interface.rd_addr_b;
      DataOutput_next <= empty ? DataOutput : mem_interface.rd_data_a;
  end
end
  
      

// --------------------------------------------
 // --------------------------------------------
 // --------------WRITE------------------------------
 // --------------------------------------------
 // --------------------------------------------
  always_ff @(posedge wr_clk or negedge rst) begin 
    if(!rst | flush) begin
         wr_ptr <= '0;
        full   <= '0;
    end else begin
        full    <= !nfull;
        wr_ptr <= nwr_ptr; 
    end
  end

       assign nwr_ptr  = (push && !full) ? wr_ptr+1'b1 : (!flush) ? wr_ptr : '0 ;
       assign we_next = (push && !full && rst) ? '1:'0;  

      assign mem_interface.we_a      = we_next;


always_ff @(posedge wr_clk or negedge rst) begin
    if(!rst | flush) begin
      mem_interface.data_a    <= '0;
      mem_interface.wr_addr_a <= 0;
    end else begin //if fifo isnt full data input is saved in the buffer
      mem_interface.data_a    <= DataInput;
      mem_interface.wr_addr_a <= (push && !empty) ? wr_ptr:'0;
  end
end
  
 //-------------------------------------------------------------------
               
  always_comb begin
      nempty = (!rst | flush) ? '1 : (wr_ptr == 'd0) ? '1 : 0;
    nfull =  (!rst | flush) ? '1 : (wr_ptr[W_ADDR] == 'b1) ? '0 : 1; 
  end          




endmodule: fifo

`endif
