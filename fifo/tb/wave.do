onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_tb/utt/wr_clk
add wave -noupdate /fifo_tb/utt/rd_clk
add wave -noupdate /fifo_tb/utt/rst
add wave -noupdate /fifo_tb/utt/push
add wave -noupdate /fifo_tb/utt/pop
add wave -noupdate -radix decimal /fifo_tb/utt/DataInput
add wave -noupdate -radix decimal /fifo_tb/utt/DataOutput
add wave -noupdate /fifo_tb/utt/peak
add wave -noupdate /fifo_tb/utt/flush
add wave -noupdate -divider flags
add wave -noupdate /fifo_tb/utt/empty
add wave -noupdate /fifo_tb/utt/full
add wave -noupdate -divider if
add wave -noupdate /fifo_tb/utt/mem_interface/we_a
add wave -noupdate -radix decimal /fifo_tb/utt/mem_interface/data_a
add wave -noupdate -radix decimal /fifo_tb/utt/mem_interface/rd_data_a
add wave -noupdate /fifo_tb/utt/mem_interface/wr_addr_a
add wave -noupdate /fifo_tb/utt/mem_interface/rd_addr_b
add wave -noupdate -divider pointers
add wave -noupdate /fifo_tb/utt/we_next
add wave -noupdate -radix decimal /fifo_tb/utt/DataOutput_next
add wave -noupdate /fifo_tb/utt/wr_ptr
add wave -noupdate /fifo_tb/utt/nwr_ptr
add wave -noupdate /fifo_tb/utt/rd_ptr
add wave -noupdate /fifo_tb/utt/nrd_ptr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {11620 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 215
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {184634 ps}
