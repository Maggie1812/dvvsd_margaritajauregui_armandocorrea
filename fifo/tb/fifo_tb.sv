`timescale 1ns / 1ps
module fifo_tb();

// Parameter Declarations
parameter DATA_WIDTH = 4;
parameter BUFFER_DEPTH = 2;

// Input Ports
bit wr_clk;
bit rd_clk;
bit rst;
bit pop;
bit push;
bit flush;
bit [DATA_WIDTH-1:0] DataInput;

// Output Ports
bit full;
bit empty;
bit [DATA_WIDTH-1:0] DataOutput;

fifo #(
    // Parameter Declarations
    .DATA_WIDTH(DATA_WIDTH),
    .BUFFER_DEPTH(BUFFER_DEPTH)
) utt (
    // Input Ports
    .wr_clk(wr_clk),
    .rd_clk(rd_clk),
    .rst(rst),
    .pop(pop),
    .push(push),
    .DataInput(DataInput),
    .flush(flush),

    // Output Ports
    .full(full),
    .empty(empty),
    .DataOutput(DataOutput)
);
   
initial begin
    wr_clk = 0;
    rd_clk = 0;
    rst = 1;
    DataInput = '0;
end

always begin
    #4 wr_clk = ~wr_clk;
end
always begin
    #2  rd_clk = ~rd_clk;
end


initial begin 
    rst = 0;    #2 
    rst = 1;

   /* repeat (BUFFER_DEPTH+1) begin
        push = 1'b1; #2
        pop  = 1'b1; #2
        #4 DataInput = DataInput + 1;
    end */
 /*repeat (BUFFER_DEPTH*2) begin
        push = 1'b1; #4
        push  = 1'b0;
        #4 DataInput = DataInput + 1; 
    end*/

    #2 DataInput = 1;
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;

    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;

    //pop
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4

    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #4
    
     flush = 1'b1;
     #4 flush = 1'b0;
   
     DataInput = 3;
    

    #8 push  = 1'b0;
    pop = 1'b1;
    #2 pop = 1'b0;
    #2
    
    pop  = 1'b0;
     push = 1'b1;
    #4 push = 1'b0;

    
     push  = 1'b0;
    pop = 1'b1;
    #4 pop = 1'b0;
    #4
     push  = 1'b0;
    pop = 1'b1;
    #4 pop = 1'b0;
    #2
    


    #800 
    $stop;
end

endmodule

