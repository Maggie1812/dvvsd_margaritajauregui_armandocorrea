`timescale 1ns / 1ps
module fifo_tb();

// Parameter Declarations
parameter DATA_WIDTH = 5;
parameter BUFFER_DEPTH = 2;

// Input Ports
bit clk;
bit rst;
bit pop;
bit push;
bit [DATA_WIDTH-1:0] DataInput;

// Output Ports
bit full;
bit empty;
bit [DATA_WIDTH-1:0] DataOutput;

fifo #(
    // Parameter Declarations
    .DATA_WIDTH(DATA_WIDTH),
    .BUFFER_DEPTH(BUFFER_DEPTH)
) utt (
    // Input Ports
    .clk(clk),
    .rst(rst),
    .pop(pop),
    .push(push),
    .DataInput(DataInput),

    // Output Ports
    .full(full),
    .empty(empty),
    .DataOutput(DataOutput)
);
   
initial begin
    clk = 0;
    rst = 1;
    DataInput = '0;
end

always begin
        #2  clk = ~clk;
end

initial begin 
    rst = 0;    #4 
    rst = 1;

    #2 DataInput = 1;
    pop  = 1'b0;
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;

    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;

    //pop
    push  = 1'b0;
    pop = 1'b1;
    #4 pop = 1'b0;
    #4
    
    pop = 1'b1;
    #4 pop = 1'b0;
    #4

    pop = 1'b1;
    #4 pop = 1'b0;
    #4
 
    push = 1'b1;
    #4 push = 1'b0;
    #4 DataInput = DataInput+1;

    pop = 1'b1;
    #4 pop = 1'b0;
    #4
    
    pop = 1'b1;
    #4 pop = 1'b0;
    #4
    
    pop = 1'b1;
    #4 pop = 1'b0;
    #4
    
    pop = 1'b1;
    #4 pop = 1'b0;
    #4
    
    pop = 1'b1;
    #4 pop = 1'b0;
    #4
   
   
    #800 
    $stop;
end

endmodule

