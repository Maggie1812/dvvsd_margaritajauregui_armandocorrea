if [file exists work] {vdel -all}
vlib work
vlog +define+SIMULATION -f files.f
onbreak {resume}
set NoQuitOnFinish 1
#vsim -voptargs=+acc work.tb_operators
vsim -voptargs=+acc tb_mdr
do wave.do
run -all
