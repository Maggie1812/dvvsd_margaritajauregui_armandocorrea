onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mdr/itf/clk
add wave -noupdate /tb_mdr/uut/rst
add wave -noupdate /tb_mdr/itf/op
add wave -noupdate -radix decimal /tb_mdr/itf/data
add wave -noupdate /tb_mdr/itf/result
add wave -noupdate /tb_mdr/itf/remainder
add wave -noupdate /tb_mdr/itf/load
add wave -noupdate /tb_mdr/itf/start
add wave -noupdate /tb_mdr/itf/load_x
add wave -noupdate /tb_mdr/itf/load_y
add wave -noupdate /tb_mdr/itf/error
add wave -noupdate /tb_mdr/itf/ready
add wave -noupdate /tb_mdr/uut/dut/error_flag
add wave -noupdate /tb_mdr/uut/dut/post_op_error
add wave -noupdate -divider adsum
add wave -noupdate -radix binary /tb_mdr/uut/dut/R_new
add wave -noupdate -radix decimal /tb_mdr/uut/dut/new_Q_shift
add wave -noupdate -radix decimal /tb_mdr/uut/dut/remaining_cycles
add wave -noupdate /tb_mdr/uut/dut/R_last
add wave -noupdate -radix decimal /tb_mdr/uut/dut/R_new_unsigned
add wave -noupdate /tb_mdr/uut/dut/add_sub_module/sum_result
add wave -noupdate -divider ADDSUB
add wave -noupdate /tb_mdr/uut/dut/add_sub_module/in_a
add wave -noupdate /tb_mdr/uut/dut/add_sub_module/in_b
add wave -noupdate /tb_mdr/uut/dut/add_sub_module/sum_result
add wave -noupdate /tb_mdr/uut/dut/add_sub_module/sub_result
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1432619435 ps} 0} {{Cursor 2} {148136847 ps} 0} {{Cursor 3} {1432580468 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 261
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {148114126 ps} {148217472 ps}
