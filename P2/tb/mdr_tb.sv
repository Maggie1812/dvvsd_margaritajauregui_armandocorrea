//Time Scale
`timescale 1ns/1ps

module mdr_tb();
import mdr_pkg::*;

localparam PERIOD = 8 ;

//Signal declaration
bit     clk;
bit     rst;
op_t op;
data_dw_t data;
data_dw_t remainder;
data_dw_t result;
logic loadX;
logic loadY;
logic ready;
logic load;
logic start;
logic error;


//Unit Under Test instance
mdr uut (
    .rst(rst),
    .clk(clk),
    .load(load),
    .start(start),
    .data(data),
    .op(op),
    .ready(ready),
    .loadX(loadX),
    .loadY(loadY),
    .error(error),
    .remainder(remainder), 
    .result(result)
);

initial begin
    clk = '0;
    rst = '1;
    start = '0;
    data = 'd511;
    load = '0;
    op = ROOT;
    #(4*PERIOD) rst = 'd0;
    start = 'd1;
        #(PERIOD) start = 'd0;
    #(4*PERIOD)
        load = '1;
    #(PERIOD)
        load = '0;
    #(2*PERIOD)
        data = 0;
    #(4*PERIOD)
    #(PERIOD)

$stop();
end

always begin
    #(PERIOD/2) clk <= ~clk;
end
endmodule
