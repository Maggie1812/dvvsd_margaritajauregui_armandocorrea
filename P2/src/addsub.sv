`ifndef ADSUB
    `define ADSUB

    module addsub#(
        parameter DW = 10
    )(
        input logic[DW-1:0]     in_a,
        input logic[DW-1:0]     in_b,
        output logic [DW-1:0]   sum_result,
        output logic [DW-1:0]   sub_result
    );

        always_comb begin
                sum_result = in_a + in_b;
                sub_result = in_a - in_b;
        end
    endmodule
`endif
