//Developer: Margarita Jauregui
//Date: March 15, 2021


`ifndef MDR_PKG_SV
    `define MDR_PKG_SV
package mdr_pkg;
    localparam DW          = 8;
    localparam logic TRUE    = 1'b1;
    localparam logic FALSE   = 1'b0;
    localparam MULTIPLY      = 2'b00;
    localparam DIVIDE         = 2'b01;
    localparam ROOT          = 2'b10;

    localparam logic [DW-1:0] MOST_NEGATIVE = {1'b1,{DW-2{1'b0}}};
    localparam logic [DW-1:0] MOST_POSITIVE = {1'b0,{DW-2{1'b1}}};
    localparam logic [DW:0] NON_OVF_POS = {DW{1'b0}};
    localparam logic [DW:0] NON_OVF_NEG = {DW{1'b1}};



    typedef logic [1:0] op_t;
    typedef logic [DW-1:0] data_dw_t;
    typedef enum logic [1:0]{
        IDLE =  2'b00,
        LOADX = 2'b01,
        LOADY =  2'b10,
        EXEC = 2'b11
    } fsm_mdr_state_e;
endpackage
`endif
