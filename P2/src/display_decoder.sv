import bcd_decoder_pkg::*;

module display_decoder (
    input nible_t number_i,
    output unsigned_t display_o
);

//Evaluates input number and chooses corresponding values
 always_comb begin 
     case(number_i)
        'h0:    display_o = CERO_DISP;
        'h1:    display_o = ONE_DISP;
        'h2:    display_o = TWO_DISP;
        'h3:    display_o = THREE_DISP;
        'h4:    display_o = FOUR_DISP;
        'h5:    display_o = FIVE_DISP;
        'h6:    display_o = SIX_DISP;
        'h7:    display_o = SEVEN_DISP;
        'h8:    display_o = EIGHT_DISP;
        'h9:    display_o = NINE_DISP;
        'hA:    display_o = A_DISP;
        'hB:    display_o = B_DISP;
        'hC:    display_o = C_DISP;
        'hD:    display_o = D_DISP;
        'hE:    display_o = E_DISP;
        'hF:    display_o = F_DISP;
        default:   display_o = NONE_DISP;
    endcase
 end

endmodule
