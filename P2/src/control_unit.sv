`ifndef CONTROL_UNIT_SV
    `define CONTROL_UNIT_SV
import mdr_pkg::*;

module control_unit#(
    parameter DW = 8
)
(
    input bit rst_n,
    input bit clk,
    input logic load,
    input logic start,
    input data_dw_t data,
    input op_t op,
    output data_dw_t remaining_cycles,
    output logic ready,
    output logic op_pipo_en,
    output logic loadX,
    output logic loadY,
    output logic error,
    output logic exec,
    output logic overflow,
    output logic empty_counter
);


data_dw_t dataX;
data_dw_t dataY;
logic overflow_aux;
logic error_flag;
logic counter_en;
logic [DW-1:0]      counter;
fsm_mdr_state_e    current_state;

assign overflow = overflow_aux;
    assign remaining_cycles = DW - counter;

bin_counter_ovf #(
    .DW(DW),
    .MAXCNT(DW),
    .MAXCNT2(DW+1)
)counter_ovf(
    .clk(clk),
    .rst_n(rst_n),
    .enb(counter_en),
    .ovf(overflow_aux),
    .op(op),
    .count(counter)
);

assign error = error_flag;
assign empty_counter = (counter == 0)? 1'b1 : 1'b0;

always_comb begin : signals_fsm
if (~rst_n) 
        begin
            ready = FALSE;
            loadX = FALSE;
            loadY = FALSE;
            exec = FALSE;
            counter_en = FALSE;
            op_pipo_en = FALSE;
        end
    else 
        case(current_state)
            IDLE:  
                begin 
                    ready = TRUE;
                    loadX = FALSE;
                    loadY = FALSE;
                    exec = FALSE;
                    counter_en = FALSE;
                    op_pipo_en = TRUE;
                end
            LOADX:
                begin
                    ready = FALSE;
                    counter_en = FALSE;
                    op_pipo_en = FALSE;
                    loadX = TRUE;
                    loadY = FALSE;
                    exec = FALSE;
                end
            LOADY:
                begin
                    loadY = TRUE;
                    loadX = FALSE;
                    exec = FALSE;
                    ready = FALSE;
                    op_pipo_en = FALSE;
                    counter_en = FALSE;
                end
            EXEC:
                begin
                    counter_en = TRUE;
                    op_pipo_en = FALSE;
                    exec = TRUE;
                    ready = FALSE;
                    loadX = FALSE;
                    loadY = FALSE;
                end
        endcase
end

always_ff @( posedge clk, negedge rst_n ) begin : data_fsm
    if (~rst_n) 
        begin
            dataX = '0;
            dataY = '0;
            error_flag = FALSE;
        end
    else 
        case (current_state)
            LOADX:
                    if (load) 
                        dataX <= data;
                    else 
                        dataX <= dataX;
            LOADY:
                    if (load)
                        dataY <= data;
                    else 
                        dataY <= dataY;
            EXEC:   
                case(op)
                    MULTIPLY: 
                        begin
                                error_flag = FALSE;
                        end
                    DIVIDE:
                        begin
                            if (dataY == 0)
                                error_flag = TRUE;
                            else
                                error_flag = FALSE;
                        end
                    ROOT:
                        begin
                            if (data[DW-1] == TRUE)
                                error_flag = TRUE;
                            else
                                error_flag = FALSE;
                        end
                    default: 
                        error_flag = FALSE;
                endcase
                
            endcase
    end



always_ff @( posedge clk, negedge rst_n ) begin : states_fsm
    if (~rst_n) 
        begin
            current_state <= IDLE;
        end
    else 
        begin
            case (current_state)
                IDLE:   
                    begin 
                        if (start)
                            current_state <= LOADX;
                        else 
                            current_state <= IDLE;
                    end
                LOADX:
                    begin
                        if (load) 
                            begin
                                if (op == ROOT)    
                                    current_state <= EXEC;
                                else 
                                    current_state <= LOADY;
                            end
                        else 
                            current_state <= LOADX;
                    end
               LOADY:
                    begin
                        if (load)
                            current_state <= EXEC;
                        else 
                            current_state <= LOADY;
                    end
                EXEC:   
                    begin 
                        if (overflow)
                            current_state <= IDLE;
                        else
                            current_state <= EXEC;
                    end
            endcase
        end
    end
endmodule
`endif
