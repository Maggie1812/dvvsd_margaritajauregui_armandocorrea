//Coder: Maggie
//Date: Jan 25, 2021

import bcd_decoder_pkg::*;

module bdc_decoder #(
    parameter DW = 8
)
(
    input logic  [DW-1:0] data_i,
    output logic         sign,
    output nible_t   cents_o,
    output nible_t   decens_o,
    output nible_t   units_o
);

logic  [DW-1:0] extended_sign;
logic  [DW-1:0] absolute_value;
logic  [DW-1:0] decens_aux;
logic  [DW-1:0] units_aux; 

    always_comb begin
        sign =  data_i[DW-1];         //Shifting right 7 to get MSB = sign
        extended_sign = {DW{{sign}}};    //Replicate sign to get a mask
        absolute_value = (data_i ^ extended_sign) + sign;  //By executing XOR with the mask obtained and adding the sign, 
                                                                //we're able to get the number's absolute value 
       
        //Getting corresponding values for units, decens and cents
        units_aux = absolute_value%10;
        decens_aux = (absolute_value%100) - units_aux;
        cents_o = (absolute_value >= 100)? 'd1:'0;

        //Get upper number in the decens
        case (decens_aux)
            CERO:       decens_o = '0;
            TEN:        decens_o = 'd1;
            TWENTY:     decens_o = 'd2;
            THIRTY:     decens_o = 'd3;
            FOURTY:     decens_o = 'd4;
            FIFTY:      decens_o = 'd5;
            SIXTY:      decens_o = 'd6;
            SEVENTY:    decens_o = 'd7;
            EIGHTY:     decens_o = 'd8;
            NINETY:     decens_o = 'd9;
            default:    decens_o = '0;
        endcase

    end

    
    assign units_o = units_aux;

endmodule