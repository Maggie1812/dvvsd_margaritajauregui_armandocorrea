
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow

module bin_counter_ovf #(
parameter 
DW      = 4, 
MAXCNT  = 5,
MAXCNT2 = 6
)(
input           clk,
input           rst_n,
input           enb,
input   [1:0]   op,
output logic    ovf,
output [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;
logic  [DW+1:0]    mux_maxcnt;

mux_2_1#(
    .DW(DW+1)
)maxcnt_mux(
    .in_a(MAXCNT),
    .in_b(MAXCNT2),
    .sel(op[1]),
    .out(mux_maxcnt)
);
always_ff@(posedge clk, negedge rst_n)begin: counter
    if (!rst_n)
        count_r     <=  '0          ;
    else if (enb)
        count_r     <= count_nxt    ;
    else 
        count_r     <= '0           ;
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
   count_nxt = count_r + 1'b1 ;
    if (count_r == mux_maxcnt-1'b1)
        ovf     =   1'b1;    
    else
        ovf     =   1'b0;
end

assign count    =   count_r;

endmodule
