// Coder:       Abisai Ramirez Perez
// Date:        June 2th, 2019
// Name:        cntr_mod_n_ovf.sv
// Description: This is a counter mod-n with a flag for indicating 
//              overflow.


`ifndef CNTR_MOD_N_OVF_SV
    `define CNTR_MOD_N_OVF_SV
// This is a Mod-n counter with overflow indication and its package. 
module cntr_mod_n_ovf #(
// clk frequency
parameter real FREQ     = 50_000_000,
// Desired delay: usually 20 - 30ms
parameter real DLY      = 0.03,
// The following two parameters are calulated
parameter int MAXCNT    = ( DLY*FREQ ), 	//This parameter could break your elaboration
parameter        DW     = $clog2(MAXCNT)
)(
input               clk,
input               rst,
input               enb,
output 	logic       ovf,
output  [DW-1:0]    count
);


typedef logic [DW-1:0] cnt_t;
typedef logic          ovf_t;

typedef struct {
cnt_t       count;
ovf_t       ovf;
} cntr_t;

cntr_t   cntr;
cnt_t    cntr_nx;

always_ff@(posedge clk, negedge rst) begin: counter
    if (!rst)
        cntr.count    <=  '0;
    else if (enb)
        cntr.count    <= cntr_nx;
end:counter

always_comb begin: comparator
    cntr.ovf     =   (cntr.count >= (MAXCNT - 1'b1) ); 
    if (cntr.count >= (MAXCNT-1'b1) )
        cntr_nx    <= '0;
    else
        cntr_nx    <= cntr.count + 1'b1;
end:comparator

assign count    =   cntr.count;
assign ovf      =   cntr.ovf;

endmodule

`endif
