//Date: Feb 14, 2021
`ifndef MUX_4_1_SV
    `define MUX_4_1_SV

module mux_4_1#(
    parameter DW = 4
)
(
    input logic [DW-1:0] in_a,
    input logic [DW-1:0] in_b,
    input logic [DW-1:0] in_c,
    input logic [DW-1:0] in_d,
    input logic [1:0] sel,
    output logic [DW-1:0] out
);

always_comb begin 
    case (sel)
        'b00:
            out = in_a;
        'b01:
            out = in_b;
        'b10:
            out = in_c;
        'b11:
            out = in_d;
    endcase
end 

endmodule
`endif
