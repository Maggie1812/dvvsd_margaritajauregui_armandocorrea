`ifndef POST_CHECK
    `define POST_CHECK
    import mdr_pkg::*;

    module post_check#(
        parameter DW = 10
    )(
        input logic [2*DW-1:0] result,    
        input logic [1:0] op,
        output logic error 
    );
        always_comb begin 
            case(op)
                MULTIPLY:
                    error = (result <= MOST_POSITIVE & result >= MOST_NEGATIVE) ? FALSE : TRUE;
                default:
                    error = FALSE;
            endcase
        end
    endmodule

`endif

