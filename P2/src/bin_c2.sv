`ifndef BIN_C2
    `define BIN_C2
module bin_c2#(
    parameter DW = 4
) (
    input logic [DW-1:0]      binData,
    output logic [DW-1:0]    c2Data
);

    always_comb begin
        c2Data = (~binData) + 1'b1;
    end

endmodule
`endif
