`ifndef CLOCK_GENERATOR_SV
    `define CLOCK_GENERATOR_SV
import clk_div_pkg::*;

module clock_generator
(
    input bit rst_n,
    input bit clk,
    output bit clk_div
);

logic clk_pll;


pll_50M_10k pll_inst(
    .refclk(clk),
    .rst( rst_n),
    .outclk_0(clk_pll)
);

clk_div_top#(
    .FREQUENCY_OUT(FREQUENCY_CLK_TB),
    .FREQUENCY_REF(FREQUENCY_CLK_FPGA)
)clk_divisor(
    .clk(clk_pll),
    .rst_n(~rst_n),
    .clk_gen(clk_div)
);
endmodule
`endif
