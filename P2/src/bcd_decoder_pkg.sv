//Coder: Maggie
//Date: Jan 25, 2021

`ifndef BCD_DECODER_PKG_SV
    `define BCD_DECODER_PKG_SV
package bcd_decoder_pkg;
        localparam DW = 8;
        localparam NW = 4;

        //Define constants for corresponding display bits for each hexa number
        localparam CERO_DISP = 'b01000000;
        localparam ONE_DISP = 'b01111001;
        localparam TWO_DISP = 'b00100100;
        localparam THREE_DISP = 'b00110000;
        localparam FOUR_DISP = 'b00011001;
        localparam FIVE_DISP = 'b00010010;
        localparam SIX_DISP = 'b00000010;
        localparam SEVEN_DISP = 'b01111000;
        localparam EIGHT_DISP = 'b00000000;
        localparam NINE_DISP = 'b00010000;
        localparam A_DISP = 'b01110111;
        localparam B_DISP = 'b01111100;
        localparam C_DISP = 'b00111001;
        localparam D_DISP = 'b01011110;
        localparam E_DISP = 'b01111001;
        localparam F_DISP = 'b01110001;
        localparam NONE_DISP = '0;

        //Constants to refer decens
        localparam CERO = '0;
        localparam TEN = 'd10;
        localparam TWENTY = 'd20;
        localparam THIRTY = 'd30;
        localparam FOURTY = 'd40;
        localparam FIFTY = 'd50;
        localparam SIXTY = 'd60;
        localparam SEVENTY = 'd70;
        localparam EIGHTY = 'd80;
        localparam NINETY = 'd90;
        localparam NIBLE_MASK = 'h0F;

        typedef logic signed [DW-1:0]  signed_t;
        typedef logic  [DW-1:0]  unsigned_t;
        typedef logic   [NW-1:0] nible_t;
endpackage
`endif 
