module abs_value#(
    parameter DW = 4
) (
    input logic [DW-1:0]      signedData,
    output logic [DW-1:0]    absValue
);
logic sign;

    always_comb begin
        sign =  signedData[DW-1];         //Get MSB = sign
        absValue = (signedData ^ {DW{{sign}}}) + sign;
    end

endmodule