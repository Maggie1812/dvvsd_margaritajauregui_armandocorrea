
import mdr_pkg::*;
import dbcr_pkg::*;
import bcd_decoder_pkg::*;



module mdr #(
    DW = 8
) (
    input bit rst,
    input bit clk,
    input logic load,
    input logic start,
    input data_dw_t data,
    input op_t op,
    output logic ready,
    output logic loadX,
    output logic loadY,
`ifdef SIMULATION
    output data_dw_t remainder, 
    output data_dw_t result,
`endif
    `ifndef SIMULATION
    output logic [6:0] res_cents_segments,
    output logic [6:0] res_decens_segments,
    output logic [6:0] res_units_segments,
    output logic [6:0] rem_cents_segments,
    output logic [6:0] rem_decens_segments,
    output logic [6:0] rem_units_segments,
    `endif 
    output logic error
);

logic [2*DW-1:0] MOST_NEG = {{DW+1{1'b1}},{DW-1{1'b0}}};
logic [2*DW-1:0] MOST_POS = {{DW+1{1'b0}},{DW-1{1'b1}}};
bit clk_div;
logic overflow;
logic op_pipo_en; 
logic result_sign;
logic remainder_sign;
logic [3:0] result_cents;
logic [3:0] result_decens;
logic [3:0] result_units;
logic [3:0] remainder_cents;
logic [3:0] remainder_decens;
logic [3:0] remainder_units;
op_t op_reg;
logic empty_counter;
logic [DW-1:0] x_reg;
logic [DW-1:0] x_abs;
logic [DW-1:0] x_c2_abs;
logic [DW-1:0] y_abs;
logic [DW-1:0] x_c2;
logic [DW-1:0] y_reg;
logic [DW-1:0] y_c2;
logic [2*DW-1:0] result_c2;
logic [DW-1:0] result_reg;
logic [2*DW:0] ADQ_source_data;
logic [2*DW:0] inA_source;
logic [2*DW:0] inB_source;
logic [2*DW:0] SM_source_data;
logic [2*DW:0] AS_source_data;
logic [2*DW:0] data_mux_ADreg_out;
logic [2*DW:0] exec_mux_ADreg_out;
logic [2*DW:0] source_mux_ADreg_out;
logic [2*DW:0] ADreg_out;
logic [DW-1:0] c2_data;
logic [2*DW:0] PAR_initial_source_data;
logic [2*DW:0] PAR_reg_source_data;
logic [2*DW:0] PAR_reg_data;
logic [2*DW:0] PAR_source_data;
logic [2*DW:0] sum_result;
logic [2*DW:0] sub_result;
logic [2*DW:0] newP;
logic [2*DW:0] P_source_data;
logic [2*DW:0] AQ;
logic [2*DW:0] M_source_data;
logic [2*DW:0] Q_source_data;
logic [2*DW-1:0] AQ_shifted;
logic [DW:0] A_shifted;
logic [DW:0] A_new;
logic [DW-2:0] Q_shifted;
logic [DW-1:0] Q_new;
logic [2*DW-1:0] result_mux_out;
logic [2*DW-1:0] remainder_bin_c2_data;
logic [DW:0] neg_remainder_data;
logic [2*DW-1:0] error_result_data;
logic [DW-1:0] remainder_mux_out;
logic [DW-1:0] error_remainder_data;
logic [DW-1:0] remainder_reg;
logic [2*DW-1:0] remainder_c2;
logic [2*DW-1:0] result_bin_c2_data;
logic [2*DW-1:0] D_shifted; //sqrt
logic [2*DW:0] sqrt_temp2_4;//sqrt
logic [2*DW:0] sqrt_temp1_3;//sqrt
logic [DW-1:0] R_temp; //sqrt
logic [DW-1:0] R_new; //sqrt
logic [DW-1:0] R_last; //sqrt
logic [DW-1:0] compQ; //sqrt
logic [DW-1:0] R_new_unsigned; //sqrt
logic [DW-1:0] R_bin_c2_data;//sqrt
logic [DW:0] sqrt_Q_shift; //sqrt
logic [DW:0] new_Q_shift; //sqrt
logic [DW:0] Q_sqr_out;//sqrt
logic [DW:0] Q_sqr_last_out;//sqrt
logic [DW-1:0] remaining_cycles;//sqrt
logic [DW-1:0] shift_amount;//sqrt
logic Pmsb;
logic error_flag;
logic error_reg;
logic exec;
logic post_op_error;
logic result_overflow_pos;
logic result_overflow_neg;
logic [2*DW-1:0] final_result_data;
    `ifdef SIMULATION
unsigned_t res_cents_segments;
unsigned_t res_decens_segments;
unsigned_t res_units_segments;
unsigned_t rem_cents_segments;
unsigned_t rem_decens_segments;
unsigned_t rem_units_segments;
logic rst_w;
logic load_w;
    `endif

`ifdef SIMULATION
    assign rst_w = rst;
    assign load_w = load;
`else
    assign rst_w = ~rst;
`endif

//////////////////////////////////////////////////////////////////////////////
//
//          Operation
//
//////////////////////////////////////////////////////////////////////////////


pipo #(
    .DW(2)
) 
op_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(op_pipo_en),
    .dataIn(op),
    .dataOut(op_reg)
);

assign result = result_reg;
assign remainder = remainder_reg;
assign error = error_reg;

///////////////////////////////////////////////////////////////////////////////
//
//          Control
//
//////////////////////////////////////////////////////////////////////////////


control_unit #(
    .DW(DW)
)
 control_unit(
    .rst_n(~rst_w),
    .clk(clk_div),
    .load( load_w),
    .start( multi_en),
    .data(data),
    .op(op_reg),
    .remaining_cycles(remaining_cycles),
    .ready(ready),
    .op_pipo_en(op_pipo_en),
    .loadX(loadX),
    .loadY(loadY),
    .error(error_flag),
    .exec(exec),
    .overflow(overflow),
    .empty_counter(empty_counter)
);
//////////////////////////////////////////////////////////////////////////////
//
//         Clock Generator 
//
//////////////////////////////////////////////////////////////////////////////


`ifdef SIMULATION

assign clk_div = clk;

`else

clock_generator clk_gen(
    .clk(clk),
    .rst_n( rst_w),
    .clk_div(clk_div)
);

`endif
//////////////////////////////////////////////////////////////////////////////
//
//          Debouncer
//
//////////////////////////////////////////////////////////////////////////////

`ifdef SIMULATION
assign multi_en = start;
`else

dbcr_top i_fsm_dbcr(
    .clk (clk_div),
    .rst_n(~rst_w),
    .Din( start),
    .one_shot(multi_en)
);


dbcr_top dbcr_load(
    .clk (clk_div),
    .rst_n(~rst_w),
    .Din( load),
    .one_shot(load_w)
);


`endif

///////////////////////////////////////////////////////////////////////////////
//
//          Data Path 
//
//////////////////////////////////////////////////////////////////////////////


pipo #(
    .DW(1)
) 
error_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(overflow),
    .dataIn(error_flag || post_op_error),
    .dataOut(error_reg)
);


pipo #(
    .DW(DW)
) 
x_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(loadX &&  load_w),
    .dataIn(data),
    .dataOut(x_reg)
);

pipo #(
    .DW(DW)
) 
y_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(loadY &&  load_w),
    .dataIn(data),
    .dataOut(y_reg)
);


abs_value #(
     .DW(DW)
) x_abs_value(
    .signedData(x_reg),
    .absValue(x_abs)
);


bin_c2#(
    .DW(DW)
) bin_c2_x_abs(
    .binData(x_abs),
    .c2Data(x_c2_abs)
);

bin_c2#(
    .DW(DW)
) bin_c2_x(
    .binData(x_reg),
    .c2Data(x_c2)
);


mux_2_1#(
    .DW(2*DW+1)
)Q_source_mux
(
    .in_a({{DW+1{x_reg[DW-1]}},x_reg}),
    .in_b({{DW+1{x_c2[DW-1]}},x_c2}),
    .sel(x_reg[DW-1]),
    .out(Q_source_data)
);

mux_3_1#(
    .DW(2*DW+1)
)ADQ_source_mux
(
    .in_a({x_abs, {DW+1{1'b0}}}),
    .in_b(Q_source_data),
    .in_c({{DW+1{x_reg[DW-1]}},x_reg}),
    .sel(op_reg),
    .out(ADQ_source_data)
);


bin_c2#(
    .DW(DW)
) bin_c2_y(
    .binData(y_reg),
    .c2Data(y_c2)
);


mux_2_1#(
    .DW(2*DW+1)
)M_source_mux
(
    .in_a({{DW+1{y_reg[DW-1]}},y_reg}),
    .in_b({{DW+1{y_c2[DW-1]}},y_c2}),
    .sel(y_reg[DW-1]),
    .out(M_source_data)
);

mux_2_1#(
    .DW(2*DW+1)
)SM_source_mux
(
    .in_a({x_c2_abs, {DW+1{1'b0}}}),
    .in_b(M_source_data),
    .sel(op_reg[0]),
    .out(SM_source_data)
);

abs_value #(
     .DW(DW)
) y_abs_value(
    .signedData(y_reg),
    .absValue(y_abs)
);

mux_3_1#(
    .DW(2*DW+1)
)PAR_initial_source_mux
(
    .in_a({ {DW{1'b0}}, y_abs, 1'b0}),
    .in_b({{DW+1{1'b0}}, ADQ_source_data[DW-1:0]}),
    .in_c('0),
    .sel(op_reg),
    .out(PAR_initial_source_data)
);


assign newP = {sum_result[2*DW], sum_result[2*DW:1]};


mux_3_1#(
    .DW(2*DW+1)
)PAR_reg_source_mux
(
    .in_a(newP),
    .in_b({A_new, Q_new}),
    .in_c({new_Q_shift,R_new}),
    .sel(op_reg),
    .out(PAR_reg_source_data)
);


pipo #(
    .DW(2*DW+1)
) 
WR_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(exec),             
    .dataIn(PAR_reg_source_data),
    .dataOut(PAR_reg_data)
);


mux_2_1#(
    .DW(2*DW+1)
)PAR_source_mux
(
    .in_a(PAR_reg_data),
    .in_b(PAR_initial_source_data),
    .sel(empty_counter),
    .out(PAR_source_data)
);    

assign AQ_shifted = {PAR_source_data[2*DW-1],PAR_source_data[2*DW-1:0]};
assign A_shifted = AQ_shifted[2*DW-1:DW-1];
assign Q_shifted = AQ_shifted[DW-2:0];

//sqrt wires and shifts
assign shift_amount = {remaining_cycles[DW-1:0],1'b0};//sqrt
assign D_shifted = ADQ_source_data >> shift_amount;//Right shift
assign sqrt_temp2_4 = {PAR_source_data[DW-3:0],2'b0};//sqrt
assign sqrt_temp1_3 = D_shifted & 'd3;//sqrt
assign R_temp = sqrt_temp2_4 | sqrt_temp1_3;//sqrt
assign sqrt_Q_shift = {PAR_source_data[2*DW-2:DW],2'b0};//sqrt

mux_2_1#(
    .DW(2*DW+1)
)Q_sqr_mux(
    .in_a(21'd1),
    .in_b(21'd3),
    .sel(PAR_source_data[DW-1]),
    .out(Q_sqr_out)//sqrt
);


mux_4_1#(
    .DW(2*DW+1)
)AS_source_mux
(
    .in_a('0),
    .in_b(ADQ_source_data),
    .in_c(SM_source_data),
    .in_d('0),
    .sel(PAR_source_data[1:0]),             
    .out(AS_source_data)
);



mux_2_1#(
    .DW(2*DW+1)
)P_source_mux
(
    .in_a(newP),
    .in_b(PAR_source_data),
    .sel(empty_counter),
    .out(P_source_data)
);

mux_3_1#(
    .DW(2*DW+1)
)inA_source_mux
(
    .in_a(PAR_source_data),
    .in_b({{DW{A_shifted[DW]}}, A_shifted[DW:0]}),
    .in_c({11'b0,R_temp}),
    .sel(op_reg),
    .out(inA_source)
);



mux_3_1#(
    .DW(2*DW+1)
)inB_source_mux
(
    .in_a(AS_source_data),
    .in_b(SM_source_data),
    .in_c({10'b0,{Q_sqr_out | sqrt_Q_shift}}),
    .sel(op_reg),
    .out(inB_source)
);

addsub#(
    .DW(2*DW+1)
)add_sub_module(
    .in_a(inA_source),
    .in_b(inB_source),
    .sum_result(sum_result),
    .sub_result(sub_result)
    );


mux_2_1#(
    .DW(DW+1)
)A_new_mux
(
    .in_a(sub_result[DW:0]),
    .in_b(sum_result[DW:0]),
    .sel(PAR_source_data[2*DW]),
    .out(A_new)
);

mux_2_1#(
    .DW(DW+1)
)sqrt_R_new_mux
(
    .in_a(sub_result[DW:0]),
    .in_b(sum_result[DW:0]),
    .sel(PAR_source_data[DW-1]),
    .out(R_new)
);
//sqrt
mux_2_1#(
    .DW(2*DW+1)
)Q_sqr_last_mux(
    .in_a('d1),
    .in_b('d0),
    .sel(R_new[DW-1]),
    .out(Q_sqr_last_out)
);

assign new_Q_shift = {PAR_source_data[2*DW-1:DW],1'b0} |  Q_sqr_last_out;//sqrt

mux_2_1#(
    .DW(DW)
)Q_new_mux
(
    .in_a({Q_shifted, 1'b1}),
    .in_b({Q_shifted, 1'b0}),
    .sel(A_new[DW]),
    .out(Q_new)
);

///////////////////////////////////////////////////////////////////////////////
//
//          Results
//
//////////////////////////////////////////////////////////////////////////////





mux_3_1#(
    .DW(2*DW)
)result_mux
(
    .in_a( newP[2*DW:1]),
    .in_b({{DW{Q_new[DW-1]}},Q_new}),
    .in_c(new_Q_shift),
    .sel(op_reg),
    .out(result_mux_out)
);


bin_c2#(
    .DW(2*DW)
) bin_c2_result(
    .binData(result_mux_out),
    .c2Data(result_c2)
);

mux_2_1#(
    .DW(2*DW)
)result_bin_c2_mux
(
    .in_a(result_mux_out),           //Result bin
    .in_b(result_c2),                     //Result c2
    .sel(x_reg[DW-1] ^^ y_reg[DW-1]),
    .out(result_bin_c2_data)
);

mux_2_1#(
    .DW(2*DW)
)final_result_mux
(
    .in_a(result_bin_c2_data),           //Result bin
    .in_b(result_mux_out),                     //Result c2
    .sel(op_reg[1]),
    .out(final_result_data)
);

assign result_overflow_pos = (final_result_data > MOST_POS) ? '0:'1; 
assign result_overflow_neg = (final_result_data < MOST_NEG)? '0:'1;
assign post_op_error = (~result_overflow_pos && ~result_overflow_neg  && op_reg == MULTIPLY);
 /*
assign result_overflow_pos = (final_result_data[2*DW-1:DW] == NON_OVF_POS) ? '0:'1; 
assign result_overflow_neg = (final_result_data[2*DW-1:DW] == NON_OVF_NEG)? '0:'1;
assign post_op_error = ( final_result_data[2*DW-1:DW] != NON_OVF_NEG && final_result_data[2*DW-1:DW] != NON_OVF_POS && op_reg == MULTIPLY);
 */
mux_2_1#(
    .DW(2*DW)
)error_result_mux
(
    .in_a(final_result_data),           //Result 
    .in_b('1),                     //Result error
    .sel(error_flag || post_op_error),
    .out(error_result_data)
);

pipo #(
    .DW(DW)
) 
result_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(overflow),
    .dataIn(error_result_data[DW-1:0]),
    .dataOut(result_reg)
);


mux_2_1#(
    .DW(DW+1)
)neg_remainder_mux
(
    .in_a(A_new),                              //Remainder (A)
    .in_b(A_new + SM_source_data[DW:0]) ,      //Negative remainder (A+M)
    .sel(A_new[DW]),
    .out(neg_remainder_data)
);


bin_c2#(
    .DW(2*DW)
) bin_c2_remainder(
    .binData({{DW-1{neg_remainder_data[DW]}},neg_remainder_data}),
    .c2Data(remainder_c2)
);

bin_c2#(
    .DW(DW)
) bin_c2_sqrt_R(
    .binData(R_new),
    .c2Data(R_new_unsigned)
);
assign compQ = ((new_Q_shift << 1) | 1);
assign R_last = R_new_unsigned ^ ((new_Q_shift << 1) | 1);

mux_2_1#(
    .DW(2*DW)
)remainder_bin_c2_R_mux
(
    .in_a(R_new),           
    .in_b(R_last),         
    .sel(R_new[DW-1]),
    .out(R_bin_c2_data)//sqrt remainder
);


mux_2_1#(
    .DW(2*DW)
)remainder_bin_c2_mux
(
    .in_a({{DW-1{neg_remainder_data[DW]}},neg_remainder_data}),           //Remainder bin
    .in_b(remainder_c2),                     //Remainder c2
    .sel(x_reg[DW-1]),
    .out(remainder_bin_c2_data)
);

mux_3_1#(
    .DW(DW)
)remainder_mux
(
    .in_a('0),
    .in_b(remainder_bin_c2_data[DW-1:0]),
    .in_c(R_bin_c2_data),
    .sel(op_reg),
    .out(remainder_mux_out)
);

mux_2_1#(
    .DW(DW)
)error_reminder_mux
(
    .in_a(remainder_mux_out),           //Result bin
    .in_b('1),                     //Result c2
    .sel(error_flag || post_op_error),
    .out(error_remainder_data)
);


pipo #(
    .DW(DW)
) 
remainder_pipo(
    .clk(clk_div),
    .rst_n(~rst_w),
    .en(overflow),
    .dataIn(error_remainder_data),
    .dataOut(remainder_reg)
);


///////////////////////////////////////////////////////////////////////////////
//
//          BCD Decoding
//
//////////////////////////////////////////////////////////////////////////////


bdc_decoder#(
    .DW(DW)
) result_decoder(
    .data_i(result_reg),
    .sign(result_sign),
    .cents_o(result_cents),
    .decens_o(result_decens),
    .units_o(result_units)
);

display_decoder res_cents_display(
    .number_i(result_cents),
    .display_o(res_cents_segments)
);

display_decoder res_decens_display(
    .number_i(result_decens),
    .display_o(res_decens_segments)
);

display_decoder res_units_display(
    .number_i(result_units),
    .display_o(res_units_segments)
);

bdc_decoder #(
    .DW(DW)
) remainder_decoder(
    .data_i(remainder_reg),
    .sign(remainder_sign),
    .cents_o(remainder_cents),
    .decens_o(remainder_decens),
    .units_o(remainder_units)
);

display_decoder rem_cents_display(
    .number_i(remainder_cents),
    .display_o(rem_cents_segments)
);

display_decoder rem_decens_display(
    .number_i(remainder_decens),
    .display_o(rem_decens_segments)
);

display_decoder rem_units_display(
    .number_i(remainder_units),
    .display_o(rem_units_segments)
);
endmodule
