//Time Scale
`timescale 1ns/1ps

module sequential_multiplier_tb();
import sequential_multiplier_pkg::*;

localparam PERIOD = 8 ;

//Signal declaration
bit     clk;
bit     rst_n;
logic ready;
logic start;
output_data_t product;


//Unit Under Test instance
sequential_multiplier uut (
    .clk(clk),
    .rst_n(rst_n),
    .start(start),
    .multiplicand(4'b1100), 
    .multiplier(4'b0100),
    .ready(ready),
    .product(product)
);

initial begin
    clk = '0;
    rst_n = '0;
    start = '0;
    #(4*PERIOD) rst_n = 'd1;
    start = 'd1;
        #(PERIOD) start = 'd0;
end

always begin
    #(PERIOD/2) clk <= ~clk;
end

endmodule