`timescale 10ns / 1ps

module tb_uart;
    import uart_pkg::*; 

	logic 	clk;
	logic 	rst;
	logic 	serial_data_rx;
	logic	serial_output_tx;
	logic	rx_interrupt; //flag for data received
	flag_t 	clr_interrupt;
	logic 	transmit; //start tx
	data_t	parallel_data_tx; //data to transmit
	data_t	parallel_data_rx; //data received
    logic   parity_error;

uart uut (
	.clk(clk),
	.rst_n(rst),
	.serial_data_rx(serial_data_rx),
	.serial_output_tx(serial_output_tx),
	.rx_interrupt(rx_interrupt), //flag for data received
	.clr_interrupt(clr_interrupt),
	.transmit(transmit), //start tx
	.data_to_transmit(parallel_data_tx), //data to transmit
	.data_received(parallel_data_rx), //data received
    .parity_error(parity_error)
);


initial
begin
	/** Begins test*/
	clk   = 0;
	rst = 0;
	clr_interrupt = 0;
	transmit = 0;
	parallel_data_tx = '0;
	serial_data_rx = '1;

	#2 rst = 1;

	//RX TEST
	send_rx();
	delay_clk(50);

	//TX TEST
	send_tx();
	delay_clk(50);
	
	delay_clk(50);
	$stop;

end

always begin
    #1 clk <= ~clk;
end


//Send rx
task send_rx();
	data_send_rx(8'hFA);
	data_send_rx(8'h53);
	data_send_rx(8'h03);
	data_send_rx(8'hAF);
endtask

//Send tx
task send_tx();
	data_send_tx(8'hFA);
	data_send_tx(8'h53);
	data_send_tx(8'h03);
	data_send_tx(8'hAF);
endtask

//Byte to send with UART
task data_send_rx(data_t	uart_in = 8'h01);
	#2 serial_data_rx = 1'b0;

	#2 serial_data_rx = uart_in[0];
	#2 serial_data_rx = uart_in[1];
	#2 serial_data_rx = uart_in[2];
	#2 serial_data_rx = uart_in[3];

	#2 serial_data_rx = uart_in[4];
	#2 serial_data_rx = uart_in[5];
	#2 serial_data_rx = uart_in[6];
	#2 serial_data_rx = uart_in[7];

    #2 serial_data_rx = uart_in[0]^uart_in[1]^uart_in[2]^uart_in[3]^uart_in[4]^uart_in[5]^uart_in[6]^uart_in[7];//parry bit

	#2 serial_data_rx = 1'b1;

	delay_clk(50);
 #2	clr_interrupt = 1;
	#2 clr_interrupt = 0;
endtask

//Byte to send with UART
task data_send_tx(data_t	uart_in = 8'h01);
	#2 parallel_data_tx = 1'b0;

	#2 parallel_data_tx = uart_in;

	transmit = 1;
	#2 transmit = 0;
	delay_clk(50);
endtask

task delay_clk(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask



endmodule

