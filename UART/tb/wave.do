onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_uart/uut/clk
add wave -noupdate /tb_uart/uut/uart_clk
add wave -noupdate /tb_uart/uut/rst_n
add wave -noupdate /tb_uart/uut/serial_data_rx
add wave -noupdate /tb_uart/uut/clr_interrupt
add wave -noupdate /tb_uart/uut/transmit
add wave -noupdate -radix hexadecimal /tb_uart/uut/data_to_transmit
add wave -noupdate -radix hexadecimal /tb_uart/uut/data_received
add wave -noupdate /tb_uart/uut/rx_interrupt
add wave -noupdate /tb_uart/uut/parity_error
add wave -noupdate /tb_uart/uut/serial_output_tx
add wave -noupdate -divider Rx_pipo
add wave -noupdate /tb_uart/uut/paralle_data_pipo/rst_n
add wave -noupdate /tb_uart/uut/paralle_data_pipo/en
add wave -noupdate -radix hexadecimal /tb_uart/uut/paralle_data_pipo/dataIn
add wave -noupdate /tb_uart/uut/paralle_data_pipo/dataOut
add wave -noupdate -divider RX_sipo
add wave -noupdate /tb_uart/uut/tx_piso/enb
add wave -noupdate /tb_uart/uut/tx_piso/l_s
add wave -noupdate /tb_uart/uut/tx_piso/inp
add wave -noupdate /tb_uart/uut/tx_piso/out
add wave -noupdate /tb_uart/uut/tx_pkg
add wave -noupdate -divider ctrl_unit
add wave -noupdate /tb_uart/uut/uart_control/transmit
add wave -noupdate /tb_uart/uut/uart_control/clr_interrupt
add wave -noupdate /tb_uart/uut/uart_control/serial_input
add wave -noupdate /tb_uart/uut/uart_control/ls_piso
add wave -noupdate /tb_uart/uut/uart_control/rx_interrupt_signal
add wave -noupdate -color {Violet Red} /tb_uart/uut/uart_control/reg_enable
add wave -noupdate /tb_uart/uut/uart_control/tx_enable
add wave -noupdate /tb_uart/uut/uart_control/rx_enable
add wave -noupdate /tb_uart/uut/uart_control/rx_interrupt
add wave -noupdate /tb_uart/uut/uart_control/tx_ovf
add wave -noupdate /tb_uart/uut/uart_control/rx_ovf
add wave -noupdate /tb_uart/uut/uart_control/tx_count
add wave -noupdate /tb_uart/uut/uart_control/uart_state
add wave -noupdate -divider Rx_counter
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2310760 ps} 0} {{Cursor 2} {4330000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 206
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {2129949 ps} {2399341 ps}
