// If this macro is not defined, 
`ifndef REGS_PKG_SV     
// defines the macro and avoid code recompilation.
  `define REGS_PKG_SV   
package regs_pkg;

// Definition of a local parameter
localparam W_SHIFT = 5;
// Local param for data type in piso_msb
localparam W_PISO_MSB = 6;

// Defines a data type used in the design of a shift register.
typedef logic [W_SHIFT:0] shift_bus_t;

// Data type for piso msb
typedef logic [W_PISO_MSB-1:0] piso_data_t;

endpackage
`endif

