`ifndef BAUD_GEN
    `define BAUD_GEN

    module baud_gen(
    input logic cclk,
    input logic rst_n,
    output logic uart_clk
);
    localparam DIV = 1302;
    localparam DW = $clog2(DIV)+1;
    logic [DW-1:0] clk_cntr;
    logic [DW-1:0] clk_nxt;
    logic clk_track;

    always_ff @(posedge cclk or negedge rst_n)
    begin
        if(~rst_n) begin 
            clk_cntr <= 0;
            clk_track <= 1'b0;
        end
        else if(clk_nxt == DIV)
        begin
            clk_cntr <= 0;
            clk_track <= ~clk_track;
        end
        else
            clk_cntr <= clk_nxt;
    end

    assign clk_nxt =clk_cntr + 13'b1; 
    assign uart_clk = clk_track;

endmodule
`endif
