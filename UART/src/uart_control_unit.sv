import uart_pkg::*;

module uart_control_unit(
    input bit clk,
    input bit rst_n,
    input flag_t transmit,
    input flag_t clr_interrupt,
    input logic serial_input,
    output flag_t ls_piso,
    output logic rx_interrupt_signal,
    output flag_t reg_enable,
    output flag_t  tx_enable,
    output flag_t rx_enable,
    output logic rx_ready
    );

logic rx_interrupt;
logic tx_ovf;
logic rx_ovf;
flag_t tx_enable_w;
flag_t rst_flag;
logic[DATA_SIZE-1:0] tx_count;
fsm_uart_state_e uart_state;


    assign tx_enable = tx_enable_w;


always_ff @( posedge clk, negedge rst_n ) begin : states_fsm
    if (~rst_n)begin
        uart_state <= IDLE;
        rst_flag <= TRUE;
	end
    else
        case(uart_state)
            IDLE:   begin
                rst_flag <=FALSE;
                if (transmit)
                    uart_state <= TRANSMIT;
                else if (serial_input == 1'b0 && rx_interrupt == FALSE && rst_flag == FALSE)
                    uart_state <= RECEIVE;
                else
                    uart_state <= IDLE;
            end
            TRANSMIT:begin
                rst_flag <=TRUE;
                if (tx_ovf)
                    uart_state <= IDLE;
                else
                    uart_state <= TRANSMIT;
                end
            RECEIVE:begin
                rst_flag <=TRUE;
                if(rx_ready)
                    uart_state <= HOLD;
                else
                    uart_state <= RECEIVE;
                end
            HOLD:begin
                rst_flag <=TRUE;
                if (clr_interrupt)
                    uart_state <= IDLE;
                else 
                    uart_state <=HOLD;
            end
            endcase
end

always_ff @( posedge clk, negedge rst_n ) begin : interrupt_ff
 
    if (~rst_n) 
        rx_interrupt_signal <= uart_pkg::FALSE;
    else
        if (clr_interrupt)
            rx_interrupt_signal <= uart_pkg::FALSE;
        else 
            if (rx_interrupt)
                rx_interrupt_signal <= uart_pkg::TRUE;
            else 
                rx_interrupt_signal <= uart_pkg::FALSE;
end 

always_comb begin : signals_fsm
     if (~rst_n)
     begin
         rx_enable = FALSE;
         ls_piso = FALSE;
         rx_interrupt = FALSE;
         reg_enable = FALSE;
         tx_enable_w = FALSE;
         rx_ready = FALSE;
    end
    else
        case(uart_state)
            IDLE:   begin
                 tx_enable_w = FALSE;
                 rx_enable = FALSE;
                 ls_piso = TRUE;
                rx_interrupt = (clr_interrupt == TRUE) ? FALSE : (rx_interrupt == TRUE)?TRUE:FALSE;
                 reg_enable = TRUE;
                 rx_ready = FALSE;
            end
            TRANSMIT: begin
                 tx_enable_w = TRUE;
                 rx_ready = FALSE;
                 rx_enable = FALSE;
                 rx_interrupt = FALSE;
                 reg_enable = FALSE;
                if(tx_count <= 1)
                    ls_piso = TRUE;
                else
                     ls_piso = FALSE;
            end
    

           RECEIVE: begin
                 rx_ready = rx_ovf;
                 tx_enable_w = uart_pkg::FALSE;
                 rx_enable = uart_pkg::TRUE;
                 ls_piso = uart_pkg::TRUE;
                 rx_interrupt = (rx_ovf)? uart_pkg::TRUE :uart_pkg::FALSE;
                 reg_enable = uart_pkg::FALSE;
                end

            HOLD: begin
                 rx_enable = FALSE;
                 rx_ready = rx_ovf;
                 tx_enable_w = FALSE;
                 ls_piso = TRUE;
                 rx_interrupt = TRUE;
                 reg_enable = FALSE;

            end

            default: begin
                 rx_enable = FALSE;
                 ls_piso = FALSE;
                 rx_interrupt = FALSE;
                 reg_enable = FALSE;
                 tx_enable_w = FALSE;
                 rx_ready = FALSE;
            end

        endcase
end

bin_counter_ovf #(
    .DW(DATA_SIZE),
    .MAXCNT(DATA_SIZE+4) 
)tx_counter(
    .clk(clk),
    .rst_n(rst_n),
    .enb(tx_enable_w),
    .ovf(tx_ovf),
    .count(tx_count)
);
bin_counter_ovf #(
    .DW(DATA_SIZE),
    .MAXCNT(DATA_SIZE+3)
)rx_counter(
    .clk(clk),
    .rst_n(rst_n),
    .enb(rx_enable),
    .ovf(rx_ovf)
);
endmodule
