//Date: Apr 10, 2021

import uart_pkg::*;


module uart#(
    parameter DW = DATA_SIZE
)(
    input bit clk,
    input bit rst_n,
    input logic serial_data_rx,
    input logic clr_interrupt,
    input logic transmit,
    input data_t data_to_transmit,
    output data_t data_received,
    output flag_t rx_interrupt,
    output logic parity_error,
    output logic serial_output_tx 
);

flag_t reg_enable;
flag_t rx_interrupt_w;
flag_t rx_interrupt_reg;
flag_t clr_interrupt_w;
flag_t ls_piso;
flag_t data_received_w;
flag_t interrupt_mux_out;
logic tx_parity_bit;
logic tx_serial_data;
logic tx_serial_out;
logic rx_parity_bit;
logic serial_data_rx_reg;
logic [DATA_SIZE:0] rx_parallel_data;
data_t rx_parallel_data_reg;
logic [DATA_SIZE-1:0] rx_parallel_out;
logic [DATA_SIZE-1:0] data_to_transmit_reg;
flag_t parity_error_w;
flag_t parity_error_reg;
logic rx_pipo_enable;
logic uart_clk;
flag_t rx_enable;
flag_t tx_enable;
logic transmit_w;

assign rx_interrupt = rx_interrupt_reg;
assign serial_output_tx = tx_serial_out;
assign parity_error = parity_error_reg;
assign data_received = rx_parallel_data_reg;



assign tx_parity_bit = data_to_transmit_reg[DW-1] ^ data_to_transmit_reg[DW-2] ^ data_to_transmit_reg[DW-3] ^ data_to_transmit_reg[DW-4] ^ data_to_transmit_reg[DW-5] ^ data_to_transmit_reg[DW-6] ^ data_to_transmit_reg[DW-7] ^ data_to_transmit_reg[0];
assign rx_parity_bit = rx_parallel_data[DW] ^ rx_parallel_data[DW-1] ^ rx_parallel_data[DW-2] ^ rx_parallel_data[DW-3] ^ rx_parallel_data[DW-4] ^ rx_parallel_data[DW-5] ^ rx_parallel_data[DW-6] ^ rx_parallel_data[1];

assign parity_error_w = (rx_parallel_data[0] == rx_parity_bit)? FALSE : TRUE;

TX_PKG tx_pkg; 
assign tx_pkg.start_bit = START_BIT;
assign tx_pkg.data = data_to_transmit_reg;
assign tx_pkg.parity_bit = tx_parity_bit;
assign tx_pkg.stop_bit = STOP_BIT;


//----------------Baud Generator--------------
`ifdef SIMULATION

    assign uart_clk = clk;

`else
    baud_gen baud_generator(
        .cclk(clk),
        .rst_n(rst_n),
        .uart_clk(uart_clk)
    );
`endif

//---------------DBCRs--------------------
`ifdef SIMULATION 
    assign transmit_w = transmit;
    assign clr_interrupt_w = clr_interrupt;
`else

    dbcr_top transmit_dbcr(
        .clk(uart_clk),
        .rst_n(rst_n),
        .Din(~transmit),
        .one_shot(transmit_w)
    );
dbcr_top clr_interrupt_dbcr(
        .clk(uart_clk),
        .rst_n(rst_n),
        .Din(~clr_interrupt),
        .one_shot(clr_interrupt_w)
    );
`endif

//----------------Control--------------

uart_control_unit uart_control(
    .clk(uart_clk),
    .rst_n(rst_n),
    .transmit(transmit_w),
    .clr_interrupt(clr_interrupt_w),
    .serial_input(serial_data_rx),
    .ls_piso(ls_piso),
    .rx_interrupt_signal(rx_interrupt_w),
    .reg_enable(reg_enable),
    .tx_enable(tx_enable),
    .rx_enable(rx_enable),
    .rx_ready(rx_pipo_enable)
);


//--------------TX---------------

    pipo#(
        .DW(DATA_SIZE)
    )
    data_to_transmit_pipo(
        .clk(uart_clk),
        .rst_n(rst_n),
        .en(~tx_enable),
        .dataIn(data_to_transmit),
        .dataOut(data_to_transmit_reg)
    );


    piso_lsb #(
        .W_PISO_LSB(11)
    ) tx_piso(
        .clk(uart_clk),
         .rst_n(rst_n), 
        .enb(tx_enable),
        .l_s(ls_piso),
        .inp({tx_pkg.stop_bit, tx_pkg.parity_bit, tx_pkg.data, tx_pkg.start_bit}),
        .out(tx_serial_data)
    );

    mux_2_1 #(
        .DW(1)
    ) out_piso_mux(
    .in_a(tx_serial_data),
    .in_b(STOP_BIT),
    .sel(ls_piso),
    .out(tx_serial_out)
);

//--------------RX---------------
       pipo#(
        .DW(1)
    )
    serial_data_rx_pipo(
        .clk(uart_clk),
        .rst_n(rst_n),
        .en(TRUE),
        .dataIn(serial_data_rx),
        .dataOut(serial_data_rx_reg)
    );

    sipo #(
        .DW(DATA_SIZE + 1)
    ) rx_sipo(
        .clk(uart_clk),
        .rst_n(rst_n ^ clr_interrupt_w),
        .enb(rx_enable),
        .inp(serial_data_rx_reg),
        .out(rx_parallel_data)
    );
//TODO: Verify PIPOs enabling signals

pipo #(
    .DW(1)
) rx_interrupt_pipo(
	.clk(uart_clk),
	.rst_n(rst_n),
	.en (TRUE),
	.dataIn(rx_interrupt_w),
	.dataOut(rx_interrupt_reg)
);

pipo #(
    .DW(1)
) parity_error_pipo(
	.clk(uart_clk),
	.rst_n(rst_n),
	.en (rx_pipo_enable),
	.dataIn(parity_error_w),
	.dataOut(parity_error_reg)
);

pipo #(
    .DW(DATA_SIZE)
) paralle_data_pipo(
	.clk(uart_clk),
	.rst_n(rst_n),
	.en(rx_pipo_enable),
	.dataIn(rx_parallel_data[DATA_SIZE-1:0]),
	.dataOut(rx_parallel_data_reg)
);

endmodule
