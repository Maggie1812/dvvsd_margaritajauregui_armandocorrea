//Date: Feb 14, 2021
`ifndef MUX_2_1_SV
    `define MUX_2_1_SV

module mux_2_1#(
    parameter DW = 4
)
(
    input logic [DW-1:0] in_a,
    input logic [DW-1:0] in_b,
    input logic sel,
    output logic [DW-1:0] out
);

always_comb begin 
    if (sel)
        out = in_b;
    else
        out = in_a;
end 

endmodule
`endif
