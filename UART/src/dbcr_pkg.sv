
// Engineer:        Abisai Ramirez Perez 
// 
// Create Date:     June 6th, 2019
// Design Name: 
// Module Name:     dbcr_pkg
// Project Name:    debouncer
// Target Devices:  DE2-115
// Description:     This is package of the debouncer
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`ifndef DBCR_PKG_SV
    `define DBCR_PKG_SV
package dbcr_pkg;
//localparam FREQUENCY_TB = 100;
	localparam FREQUENCY_1HZ = 1;
	localparam FREQUENCY_10HZ = 10;
	localparam FREQUENCY_1KHZ = 1_000;
	localparam FREQUENCY_10KHZ = 10_000;
    localparam FREQUENCY_CLKDIV = FREQUENCY_10KHZ;
    localparam FREQUENCY_UART = 19_200;

	localparam REFERENCE_CLKTB = 2;
	localparam REFERENCE_CLKFPGA = 50_000_000;
	localparam REFERENCE_CLKPLL = 1_000_000;


// 
typedef enum logic [1:0]{
    BAJO = 2'b00, 
    DLY1 = 2'b01, 
    ALTO = 2'b10, 
    DLY2 = 2'b11
    } fsm_dbcr_state_e;
endpackage
`endif 

