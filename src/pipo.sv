//Date: Feb 14, 2021
`ifndef PIPO_SV
    `define PIPO_SV
import sequential_multiplier_pkg::*;

module pipo#(
    parameter DW = 4
)
(
    input bit clk,
    input logic rst_n,
    input logic en,
    input logic [DW-1:0] dataIn,
    output logic [DW-1:0] dataOut
);


always_ff @( posedge clk, negedge rst_n ) begin
    if (~rst_n) 
        dataOut <= '0;
    else
        if (en)
            dataOut <= dataIn;
        else 
        dataOut <= dataOut;
    
end
endmodule
`endif
