// Title: bin_2_bcd_disp
// Date: 28/1/2021
// Author: Armando Correa
// Mail: ie708826@iteso.mx


import bin_2_bcd_pkg::*;

module bin_2_bcd_disp
(
    input dataBcd_t     bcd_num,
    output dataSeg_t    seg_num
);

    always_comb begin
        case(bcd_num)
            4'h0:   seg_num = 7'b1000000;
            4'h1:   seg_num = 7'b1111001;
            4'h2:   seg_num = 7'b0100100;
            4'h3:   seg_num = 7'b0110000;
            4'h4:   seg_num = 7'b0011001;
            4'h5:   seg_num = 7'b0010010;
            4'h6:   seg_num = 7'b0000010;
            4'h7:   seg_num = 7'b1111000;
            4'h8:   seg_num = 7'b0000000;
            4'h9:   seg_num = 7'b0010000;
            4'hA:   seg_num = 7'b0001000;
            4'hB:   seg_num = 7'b0000011;
            4'hC:   seg_num = 7'b1000110;
            4'hD:   seg_num = 7'b0100001;
            4'hE:   seg_num = 7'b0000110;
            4'hF:   seg_num = 7'b0001110;
            default:    seg_num = 7'b1111111;
        endcase
    end
endmodule
