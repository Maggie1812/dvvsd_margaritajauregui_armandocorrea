// Engineer:        DSc Abisai Ramirez Perez 
// 
// Create Date:     June 6th, 2019
// Design Name: 
// Module Name:     dbcr_top
// Project Name:    debouncer
// Target Devices:  DE2-115
// Description:     This is the top modle of the debouncer
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`ifndef DBCR_TOP_SV
    `define DBCR_TOP_SV
    import dbcr_pkg::*;
    import clk_div_pkg::*;
module dbcr_top(
    // clk is the FPGA clock or the PLL output adjust the parameter type accordingly
    input           clk,
    // reset low active asynchronous 
    input           rst_n,
    // Input port 
    input           Din,
    // One shot output port
    output logic    one_shot
);

logic Delay30ms_ready; 
logic EnableCounter;

fsm_dbcr  i_fsm_dbcr (
    .clk            ( clk               ),
    .rst_n          ( rst_n             ),
    .Din            ( Din               ),
    .Delay30ms_ready( Delay30ms_ready   ),
    .EnableCounter  ( EnableCounter     ),
    .one_shot       ( one_shot          ) );

cntr_mod_n_ovf #(
    `ifdef SIM_ON
.FREQ(FREQUENCY_TB),
.DLY(0.03)

    `else
.FREQ(REFERENCE_CLKFPGA),
.DLY(0.03)
    `endif
) i_cntr_mod_n (
    .clk    ( clk               ),
    .rst    ( rst_n             ),
    .enb    ( EnableCounter     ),
    .ovf    ( Delay30ms_ready   ),
    .count  () );
endmodule
`endif
