// date: 28/1/2021
// author: armando correa
// mail: ie708826@iteso.mx

`ifndef BIN_2_BCD_PKG_SV
    `define BIN_2_BCD_PKG_SV
package bin_2_bcd_pkg;
    localparam DW_IN    = 4;
    localparam DW_OUT   = 7;
    localparam DW_BCD   = 4;

    typedef logic [DW_IN-1:0] dataIn_t;
    typedef logic [DW_OUT-1:0] dataSeg_t;
    typedef logic [DW_BCD-1:0] dataBcd_t;
endpackage
`endif
