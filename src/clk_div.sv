`ifndef CLK_DIV
    `define CLK_DIV

import clk_div_pkg::*;
module clk_div#(
    parameter FREQUENCY = FREQUENCY_TB,
    parameter FREQUENCY_REF = FREQUENCY_CLK_FPGA //50_000_000 
)
(
    input logic clk_fpga,
    input logic rst_n,
    output logic clk
);

    localparam DIV = FREQUENCY_REF/(2*FREQUENCY);
    localparam DW = $clog2(DIV)+1;

    logic [DW-1:0] clk_cntr;
    logic [DW-1:0] clk_nxt;
    logic clk_track;

    always_ff @(posedge clk_fpga or negedge rst_n)
    begin
        if(~rst_n) begin
            clk_track <= 1'b0;
            clk_cntr <= 0;
        end
        else if (clk_nxt == DIV) 
            begin
                clk_cntr <= 0;
                clk_track <= ~clk_track;
            end
        else
            clk_cntr <= clk_nxt;
    end

    assign clk_nxt = clk_cntr + 1;
    assign clk = clk_track;
endmodule
`endif
