// date: 31/1/2021
// author: Armando Correa
// mail: ie708826@iteso.mx
//


import bin_2_bcd_pkg::*;

module bin_2_bcd_top#(
    parameter DW = 4
)
    (
        input [DW-1:0] binIn,
        output dataSeg_t cent_seg,
        output dataSeg_t dec_seg,
        output dataSeg_t unit_seg,
        output bit sign_led
    );

    dataBcd_t cent_wire;
    dataBcd_t dec_wire;
    dataBcd_t unit_wire;

    bin_2_bcd_conv#(
        .DW(DW)
    ) bin_2_bcd
    (
        .num_bin(binIn),
        .sign(sign_led),
        .bcd_cent(cent_wire),
        .bcd_dec(dec_wire),
        .bcd_uni(unit_wire)
    );

    bin_2_bcd_disp cent_display
    (
        .bcd_num(cent_wire),
        .seg_num(cent_seg)
    );

    bin_2_bcd_disp dec_display
    (
        .bcd_num(dec_wire),
        .seg_num(dec_seg)
    );

    bin_2_bcd_disp unit_display
    (
        .bcd_num(unit_wire),
        .seg_num(unit_seg)
    );

endmodule
