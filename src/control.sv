//Date: Feb 11, 2021
`ifndef CONTROL_SV
    `define CONTROL_SV
import sequential_multiplier_pkg::*;

module control(
    input logic start,
    input bit clk,
    input bit rst_n,
    output logic en,
    output logic clean,
    output logic ready,
    output logic counter_en
);

fsm_ms_state_e    current_state;
logic [N-1:0]      counter;
logic overflow;
logic start_aux;


always_comb begin : signals_fsm
    case(current_state)
        IDLE:  
            begin 
                en = TRUE;
                ready = TRUE;
                counter_en = FALSE;
                clean = FALSE;
            end
        SHOT:
            begin
                en = FALSE;
                ready = FALSE;
                counter_en = FALSE;
                clean = FALSE;
            end
        EXEC:
            begin
                counter_en = TRUE;
                ready = FALSE;
                clean = FALSE;
                en = FALSE;

            end
        CLEAN:
            begin
                ready = FALSE;
                counter_en = FALSE;
                clean =  TRUE;
                en = FALSE;
            end
    endcase
end

bin_counter_ovf #(
    .DW(N),
    .MAXCNT(N)
)counter_ovf(
    .clk(clk),
    .rst(rst_n),
    .enb(counter_en),
    .ovf(overflow),
    .count(counter)
);

always_ff @( posedge clk, negedge rst_n ) begin : states_fsm
    if (!rst_n) 
        begin
            current_state <= IDLE;
        end
    else 
        begin
            case (current_state)
                IDLE:   
                    begin 
                        if ( start )
                            current_state <= SHOT;
                        else
                            current_state <= IDLE;
                    end
                SHOT:
                    current_state <= EXEC;
                EXEC:   
                    begin
                        if (overflow)
                            current_state <= CLEAN;
                        else 
                            current_state <= EXEC;
                    end
                CLEAN:  
                    begin    
                        current_state <= IDLE;
                    end
            endcase
        end
    end
endmodule
`endif
