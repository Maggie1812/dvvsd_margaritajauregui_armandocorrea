//Date: Feb 14, 2021
`ifndef SIGN_EXTEND_SV
    `define SIGN_EXTEND_SV
import sequential_multiplier_pkg::*;

module sign_extend(
    input input_data_t in,
    output output_data_t out
);

assign out = {{N{in[N-1]}},in};

endmodule
`endif
