module c2_bin#(
    parameter DW = 4
) (
    input logic [DW-1:0]      c2Data,
    output logic                 sign,
    output logic [2*DW-1:0]    binData
);

    logic [DW-1:0] extended_sign;
    logic [DW-1:0] abs_value;

    always_comb begin
        sign =  c2Data[DW-1];         //Get MSB = sign
        abs_value = (c2Data ^ {DW{{sign}}}) + sign;
        extended_sign = {DW{{1'b0}}};    //Replicate sign to get a mask
        binData = {extended_sign,abs_value};  //By executing XOR with the mask obtained and adding the sign, 
                                                                //we're able to get the number's absolute value 

    end

endmodule