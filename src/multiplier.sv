//Date Feb 14, 2021 
`ifndef MULTIPLIER_SV
    `define MULTIPLIER_SV
import sequential_multiplier_pkg::*;
localparam logic lsb = 'b1;
localparam logic [1:0] ENABLE_CLEAN = 'b11;
localparam logic [1:0] DISABLE_CLEAN = 'b01;
localparam logic [1:0] ENABLE_NOT_CLEAN = 'b10;
localparam logic [1:0] DISABLE_NOT_CLEAN = 'b00;

module multiplier(
    input bit clk,
    input bit rst_n,
    input logic en,
    input logic store, 
    input logic cln,
    input output_data_t multiplicand, 
    input output_data_t multiplier,
    output output_data_t product
);

    logic check_bit;
    output_data_t multiplier_reg;
    output_data_t multiplicand_reg;
    output_data_t product_aux;
	 output_data_t product_aux_nxt;
    output_data_t multiplier_mux_out;
    output_data_t multiplicand_mux_out;
    output_data_t multiplicand_aux;
    output_data_t multiplier_aux;

 mux_2_1 multiplier_mux(
     .in_a(multiplier),
     .in_b(multiplier_aux),
     .out(multiplier_mux_out),
     .sel(en)
 );

pipo #(.DW(DW)) multiplier_pipo
(
    .clk(clk),
    .rst_n(rst_n),
    .en(store),
    .dataIn(multiplier_mux_out),
    .dataOut(multiplier_reg)
);

 mux_2_1 multiplicand_mux(
     .in_a(multiplicand),
     .in_b(multiplicand_aux),
     .out(multiplicand_mux_out),
     .sel(en)
 );

pipo #(.DW(DW))multiplicand_pipo(
    .clk(clk),
    .rst_n(rst_n),
    .en(store),
    .dataIn(multiplicand_mux_out),
    .dataOut(multiplicand_reg)
);


always_ff @( posedge clk, negedge rst_n) begin
    if (~rst_n)
        product_aux <= '0;
    else
           case({en,cln}) 
            ENABLE_CLEAN: product_aux <= product_aux;
            DISABLE_CLEAN: product_aux <= product_aux_nxt;
            ENABLE_NOT_CLEAN: product_aux <= product_aux_nxt;
            DISABLE_NOT_CLEAN: product_aux <= product_aux_nxt;
        endcase     
         
end

always_comb  begin 
    if (~rst_n)
    begin
        multiplicand_aux = '0;
        multiplier_aux = '0;
	    product_aux_nxt = '0;
    end
    else 
        case({en,cln}) 
            ENABLE_CLEAN: begin 
                multiplier_aux = '0;
                multiplicand_aux = '0;
                product_aux_nxt = '0;
            end
            DISABLE_CLEAN: begin 
                multiplier_aux = '0;
                multiplicand_aux = '0;
	    		product_aux_nxt = '0;

            end
            ENABLE_NOT_CLEAN: begin 
                product_aux_nxt = ( multiplier_reg[0] && lsb )? (product_aux + multiplicand_reg) : (product_aux + 0);
                multiplier_aux = multiplier_reg >> 1;
                multiplicand_aux = multiplicand_reg << 1;
            end
            DISABLE_NOT_CLEAN: begin 
				product_aux_nxt = '0;
                multiplier_aux = '0;
				multiplicand_aux = '0;
            end
				default: begin
                multiplier_aux = '0;
                multiplicand_aux = '0;
				product_aux_nxt = '0;
				end
        endcase     
     end

assign product = product_aux;

endmodule
`endif
