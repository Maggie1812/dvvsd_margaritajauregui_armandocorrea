`ifndef BIN_C2
    `define BIN_C2
module bin_c2#(
    parameter DW = 4
) (
    input logic [DW-1:0]      binData,
    input logic                 sign,
    output logic [DW-1:0]    c2Data
);

    logic [DW-1:0] extended_sign;

    always_comb begin
        extended_sign = {DW{{sign}}}; 
        c2Data = (binData ^ extended_sign) + sign;
    end

endmodule
`endif
