//Date: Feb 11, 2021

`ifndef SEQUENTIAL_MULTIPLIER_PKG_SV
    `define SEQUENTIAL_MULTIPLIER_PKG_SV
package sequential_multiplier_pkg;
    localparam N = 4;
    localparam DW = 2*N;
    localparam bit TRUE     =1'b1;
    localparam bit FALSE    =1'b0;
    localparam logic [N-1:0]  COUNTER_MAX = 'h1FF;
    localparam logic [N-1:0]  COUNTER_EMPTY = '0;

    
    typedef enum logic [1:0]{
        IDLE =  2'b00,
        SHOT = 2'b01,
        EXEC =  2'b10,
        CLEAN = 2'b11
    } fsm_ms_state_e;
    typedef logic [N-1:0]    input_data_t;
    typedef logic [DW-1:0]   output_data_t;


endpackage
`endif 

