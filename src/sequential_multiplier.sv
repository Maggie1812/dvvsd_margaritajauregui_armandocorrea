//Date: Feb 14, 2021

`ifndef SEQUENTIAL_MULTIPLIER_SV
    `define SEQUENTIAL_MULTIPLIER_SV
import sequential_multiplier_pkg::*;
import dbcr_pkg::*;
    import bin_2_bcd_pkg::*;
module sequential_multiplier(
    input bit clk,
    input bit rst_n,
    input logic start,
    input input_data_t multiplicand, 
    input input_data_t multiplier,
    output logic ready,
    output output_data_t product,
    output logic product_sign,
    output dataSeg_t product_unit_seg,
    output dataSeg_t product_dec_seg
);

output_data_t multiplicand_extended;
output_data_t multiplier_extended;
output_data_t multiplicand_reg;
output_data_t multiplier_reg;
output_data_t product_reg;
logic clean;
logic multi_en;
logic counter_en;
logic Delay30ms_ready;
logic EnableCounter;
logic one_shot;
logic sign_aux;
logic multiplier_sign;
logic [2*DW-1:0] multiplier_bin;
logic multiplicand_sign;
logic [2*DW-1:0] multiplicand_bin;
logic [DW-1:0] product_c2;
logic product_sign_xor;
logic product_sign_reg;
bit clk_pll;
bit clk_div;


//////////////////////////////////////////////////////////////////////////////
//
//         PLL and Clk divisor 
//
//////////////////////////////////////////////////////////////////////////////

`ifdef SIM_ON
assign clk_pll = clk;
`else

pll_50M_10k pll_inst(
    .refclk(clk),
    .rst(!rst_n),
    .outclk_0(clk_pll)
);
`endif

`ifdef SIM_ON
assign clk_div = clk_pll;
`else
clk_div_top#(
    .FREQUENCY_OUT(FREQUENCY_CLK_TB),
    .FREQUENCY_REF(FREQUENCY_PLL)
)clk_divisor(
    .clk(clk_pll),
    .rst_n(rst_n),
    .clk_gen(clk_div)
);
`endif
//////////////////////////////////////////////////////////////////////////////
//
//          Debouncer
//
//////////////////////////////////////////////////////////////////////////////

`ifdef SIM_ON

assign multi_en = start;
`else

dbcr_top i_fsm_dbcr(
    .clk (clk),
    .rst_n(rst_n),
    .Din(start),
    .one_shot(multi_en)
);

`endif



///////////////////////////////////////////////////////////////////////////////
//
//         C2 -> Bin + Sign extender
//
//////////////////////////////////////////////////////////////////////////////

c2_bin#(
    .DW(N)
) multiplicand_c2_bin(
    .c2Data(multiplicand),
    .sign(multiplicand_sign),
    .binData(multiplicand_bin)
);


c2_bin#(
    .DW(N)
) multiplier_c2_bin(
    .c2Data(multiplier),
    .sign(multiplier_sign),
    .binData(multiplier_bin)
);

///////////////////////////////////////////////////////////////////////////////
//
//          Control
//
//////////////////////////////////////////////////////////////////////////////


control control_unit(

    .start(multi_en),
    .clk (clk),
    .rst_n (rst_n), 
    .clean(clean),
    .en (en),
    .counter_en(counter_en),
    .ready (ready)
);

///////////////////////////////////////////////////////////////////////////////
//
//          PIPOS
//
//////////////////////////////////////////////////////////////////////////////


pipo #(
    .DW(DW)
)
multiplicand_pipo(
    .clk(clk),
    .rst_n(rst_n),
    .en(en),
    .dataIn(multiplicand_bin),
    .dataOut(multiplicand_reg)
);


pipo #(
    .DW(DW)
)
multiplier_pipo(
    .clk(clk),
    .rst_n(rst_n),
    .en(en),
    .dataIn(multiplier_bin),
    .dataOut(multiplier_reg)
);


assign product_sign_xor = multiplier[N-1] ^^ multiplicand[N-1];
pipo #(
    .DW(1)
)
sign_pipo(
    .clk(clk),
    .rst_n(rst_n),
    .en(en),
    .dataIn(product_sign_xor),
    .dataOut(product_sign_reg)
);
///////////////////////////////////////////////////////////////////////////////
//
//         Multiplier
//
//////////////////////////////////////////////////////////////////////////////


multiplier multiplier_module(
    .clk(clk),
    .rst_n(rst_n),
    .en(counter_en),
    .store(~en),
    .cln(clean),
    .multiplicand(multiplicand_reg), 
    .multiplier(multiplier_reg),
    .product(product_reg)
);

///////////////////////////////////////////////////////////////////////////////
//
//         Bin -> C2
//
//////////////////////////////////////////////////////////////////////////////


bin_c2#(
    .DW(DW)
) multiplier_bin_c2(
    .binData(product_reg),
    .sign(product_sign_reg),
    .c2Data(product_c2)
);
///////////////////////////////////////////////////////////////////////////////
//
//         Product PIPO
//
//////////////////////////////////////////////////////////////////////////////

pipo #(
    .DW(DW)
)product_pipo(
    .clk(clk),
    .rst_n(rst_n),
    .en(clean || overflow),
    .dataIn(product_c2),
    .dataOut(product)
);

////////////////////////////////////////////////////////////////////////////////
//
//          BCD converters
//
//////////////////////////////////////////////////////////////////////////////

bin_2_bcd_top#(
    .DW(DW)
) product_bcd(
    .binIn(product),
    .cent_seg(),
    .dec_seg(product_dec_seg),
    .unit_seg(product_unit_seg),
    .sign_led(sign_aux)
);

assign product_sign = ~sign_aux;
endmodule
`endif
