//Date: Feb 14, 2021
`ifndef MUX_2_1_SV
    `define MUX_2_1_SV
import sequential_multiplier_pkg::*;

module mux_2_1(
    input output_data_t in_a,
    input output_data_t in_b,
    input logic sel,
    output output_data_t out
);

always_comb begin 
    if (sel)
        out = in_b;
    else
        out = in_a;
end 

endmodule
`endif
