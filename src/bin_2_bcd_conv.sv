// Title: bin_2_bcd_conv
// Date: 28/1/2021
// Author: Armando Correa
// Mail: ie708826@iteso.mx

import bin_2_bcd_pkg::*;

module bin_2_bcd_conv#(
    parameter DW = 4
)
(
    input [DW-1:0]     num_bin,
    output              sign,
    output dataBcd_t    bcd_cent,
    output dataBcd_t    bcd_dec, 
    output dataBcd_t    bcd_uni
);
assign sign = num_bin[DW-1];//save the last bit of bin input, that represents the sign of number
logic [DW-1:0] num_ubin;
assign num_ubin = (num_bin[DW-1] == 1'b1) ? (~num_bin + 1'b1) : num_bin; //we check if number is negative to do complement
    
//rigth shift 12 spaces is a division of 4096. When you want to divide by 100 you need to divide that by 4096/100 which is 40.96 or 41  
assign bcd_cent = ((num_ubin * 'd41) >> 'd12);
	
//rigth  shift 12 spaces is a division of 4096. When you want to divide by 10 you need to divide that by 4096/10 which is 404.8 or 410  
assign bcd_dec = (((num_ubin - (bcd_cent * 'd100)) * 'd410) >> 'd12);


//For obtaining the units, we subtract the hundreds of unit and the tens of unit from the original value. 
assign bcd_uni = num_ubin - (bcd_cent * 8'd100) - (bcd_dec * 4'd10);
endmodule
