`ifndef CLK_DIV_TOP_SV
    `define CLK_DIV_TOP_SV

import clk_div_pkg::*;
module clk_div_top#(
    parameter FREQUENCY_REF = FREQUENCY_CLK_FPGA,
    parameter FREQUENCY_OUT = FREQUENCY_TB
)
(
    input logic clk,
    input logic rst_n,
    output logic clk_gen
);

    clk_div #(
    .FREQUENCY (FREQUENCY_OUT),
    .FREQUENCY_REF (FREQUENCY_REF)
) clk_divider(
    .clk_fpga(clk),
    .rst_n(rst_n),
    .clk(clk_gen)
);
endmodule

`endif
