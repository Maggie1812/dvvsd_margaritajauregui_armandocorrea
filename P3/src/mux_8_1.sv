`ifndef MUX_8_1_SV
    `define MUX_8_1_SV

module mux_8_1#(
    parameter DW = 4
)
(
    input logic [DW-1:0] in_a,
    input logic [DW-1:0] in_b,
    input logic [DW-1:0] in_c,
    input logic [DW-1:0] in_d,
    input logic [DW-1:0] in_e,
    input logic [DW-1:0] in_f,
    input logic [DW-1:0] in_g,
    input logic [DW-1:0] in_h,
    input logic [2:0] sel,
    output logic [DW-1:0] out
);

always_comb begin 
    case (sel)
        3'b000: out = in_a;
        3'b001: out = in_b;
        3'b010: out = in_c;
        3'b011: out = in_d;
        3'b100: out = in_e;
        3'b101: out = in_f;
        3'b110: out = in_g;
        3'b111: out = in_h;
        default: out = '0;
    endcase
end 

endmodule
`endif
