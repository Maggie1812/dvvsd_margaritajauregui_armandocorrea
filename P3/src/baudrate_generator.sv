`ifndef BAUDRDATE_GENERATOR_SV
    `define BAUDRATE_GENERATOR_SV
import clk_div_pkg::*;

module baudrate_generator#(
    parameter BAUDRATE = UART_BAUDRATE_19200,
    parameter FREQUENCY_REF = FREQUENCY_CLK_FPGA //50_000_000 
)
(
    input bit rst_n,
    input bit clk,
    output bit clk_div
);

localparam integer BAUDRATE_COUNT = ((FREQUENCY_CLK_FPGA) / (2*BAUDRATE));
localparam COUNT_SIZE = $clog2(BAUDRATE_COUNT);

logic baudrate_gen_ovf;
logic [COUNT_SIZE:0] count;


always_ff @( posedge clk, negedge rst_n ) begin 
    if (~rst_n) 
        clk_div <= 1'b0;
    else
        if (baudrate_gen_ovf)
            clk_div <= ~clk_div;
end




cntr_mod_n_ovf #(
    .MAXCNT( BAUDRATE_COUNT ),
    .DW(COUNT_SIZE+1)
) baudrate_counter(
    .clk(clk),
    .rst(rst_n),
    .enb(1'b1),
    .ovf(baudrate_gen_ovf),
    .count(count)
);

endmodule
`endif
