// Coder:       Abisai Ramirez Perez
// Date:        June 2th, 2019
// Name:        cntr_mod_n_ovf.sv
// Description: This is a counter mod-n with a flag for indicating 
//              overflow.


`ifndef CNTR_MOD_N_OVF_RECONFIG_SV
    `define CNTR_MOD_N_OVF_RECONFIG_SV
// This is a Mod-n counter with overflow indication and its package. 
module cntr_mod_n_ovf_reconfig #(
parameter        DW     = 4
)(
input               clk,
input               rst_n,
input   logic       enb,
input   logic [DW-1:0] maxcnt,
output 	logic       ovf,
output  [DW-1:0]    count
);


typedef logic [DW-1:0] cnt_t;
typedef logic          ovf_t;
cnt_t maxcnt_fixed;

typedef struct {
cnt_t       count;
ovf_t       ovf;
} cntr_t;

cntr_t   cntr;
cnt_t    cntr_nx;

always_ff@(posedge clk, negedge rst_n) begin: counter
    if (!rst_n ) begin
        cntr.count    <=  '0;
        maxcnt_fixed <= maxcnt;
    end
        
    else if (enb) begin
        cntr.count    <= cntr_nx;
        maxcnt_fixed <= maxcnt_fixed;
        end 
    else begin
        cntr.count    <=  '0;
        maxcnt_fixed <= maxcnt;
    end

        
end:counter

always_comb begin: comparator
    cntr.ovf     =   (cntr.count >= (maxcnt_fixed - 1'b1) ); 
    if (cntr.count >= (maxcnt_fixed-1'b1) )
        cntr_nx    <= '0;
    else
        cntr_nx    <= cntr.count + 1'b1;
end:comparator

assign count    =   cntr.count;
assign ovf      =   cntr.ovf;

endmodule

`endif
