`ifndef CLK_DIV_PKG_SV
    `define CLK_DIV_PKG_SV

package clk_div_pkg;
    localparam FREQUENCY_CLK_FPGA = 50_000_000;
    localparam FREQUENCY_TB = 100;
    localparam FREQUENCY_CLK_TB = 2_000;
    localparam FREQUENCY_PLL = 10_000_000;
    localparam UART_BAUDRATE_9600 = 9_600;
    localparam UART_BAUDRATE_115200 = 115_200;
    localparam UART_BAUDRATE_19200 = 19_200;

endpackage
`endif
