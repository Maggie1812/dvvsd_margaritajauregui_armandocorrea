module piso_msb #(
//parameter type DATA_T = regs_pkg::piso_data_t // Syntax not supported by Quartus
parameter W_PISO_MSB = regs_pkg::W_PISO_MSB
) (
input                         clk,    // Clock
input                         rst_n,    // asynchronous reset low active 
input                         enb,    // Enable
input                         l_s,    // load or shift
input logic [W_PISO_MSB-1:0]  inp,    // data input
output                        out     // Serial output
);

typedef logic [W_PISO_MSB-1:0]   piso_data_t;

piso_data_t     rgstr_r     ;
piso_data_t     rgstr_nxt   ;

// Combinational module
always_comb begin
    if (l_s)
        rgstr_nxt  = inp;
    else 
        rgstr_nxt  = {rgstr_r[W_PISO_MSB-2:0], rgstr_r[W_PISO_MSB-1]};
end

always_ff@(posedge clk or negedge rst_n) begin: rgstr_label
    if(~rst_n)
        rgstr_r     <= '0           ;
    else if (enb) begin
        rgstr_r     <= rgstr_nxt    ;
    end
end:rgstr_label

assign out  = rgstr_r[W_PISO_MSB-1];    // MSB bit is the first to leave
//######################################################################################################
//TODO: try to design a piso register, where the LSB bit leave the register first and then the LSB bit+1.
//######################################################################################################
endmodule
