import uart_pkg::*;

module ififo_set(
    input bit rd_clk, 
    input bit wr_clk,
    input bit rst_n,
    input logic ctl_push,
    input logic ctl_pop, 
    input logic data_push,
    input logic data_pop,
    input byte ctl_dataIn,
    input logic flush,
    input byte data_dataIn,
    output byte ctl_dataOut,
    output byte data_dataOut,
    output logic ctl_empty,
    output logic data_empty,
    output logic ctl_full,
    output logic data_full
);


fifo #(
    .DATA_WIDTH(BYTE_SIZE), 
    .BUFFER_DEPTH(16)
)control_fifo(
    .wr_clk(wr_clk),   
    .rd_clk(rd_clk),   
    .rst(rst_n),   
    .push(ctl_push),  
    .pop(ctl_pop),  
    .flush(flush),  
    .DataInput(ctl_dataIn),
    .DataOutput(ctl_dataOut),
    .empty(ctl_empty),
    .full(ctl_full)  
  );


fifo #(
    .DATA_WIDTH(BYTE_SIZE), 
    .BUFFER_DEPTH(16)
)data_fifo(
    .wr_clk(wr_clk),   
    .rd_clk(rd_clk),   
    .rst(rst_n),   
    .push(data_push),  
    .pop(data_pop),
    .flush(flush),  
    .DataInput(data_dataIn),
    .DataOutput(data_dataOut),
    .empty(data_empty),
    .full(data_full)  
  );
endmodule
