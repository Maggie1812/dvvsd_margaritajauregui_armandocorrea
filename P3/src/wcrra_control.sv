import wcrra_pkg::*;

module wcrra_control(
    input bit clk,
    input bit rst_n,
    input logic ack,
    output logic valid
);



always_ff @( posedge clk, negedge rst_n ) begin :_control
    if (~rst_n) begin
        valid <= wcrra_pkg::FALSE;
    end
    else begin
        valid <= wcrra_pkg::TRUE;
    end
       
end

endmodule
