import wcrra_pkg::*;
import qsys_pkg::*;
import uart_pkg::BYTE_SIZE;
module qsys_control(
    input bit cclk,
    input bit uclk,
    input bit rst_n,
    input byte UART_data,
    input byte L_reg,
    input logic rx_interrupt,
    output logic clr_interrupt,
    output logic fifo_flush,
    output logic process_en,
    output logic L_pipo_en,
    output logic data_pipo_en,
    output logic M_pipo_en,
    output logic CMD_pipo_en,
    output logic SRC_pipo_en,
    output logic DEST_pipo_en,
    output logic FIFO_data_push,
    output logic FIFO_ctl_push,
    output logic FIFO_ctl_pop,
    output logic req_cntr_en,
    output logic rtx_en
);

byte count;
byte package_size;
fsm_cmds_state_e cmds_state;
fsm_wcrra_states_e wcrra_state;
logic capture_en_aux;
logic rtx_aux;
logic L_pipo_en_aux;
logic CMD_pipo_en_aux;
logic M_pipo_en_aux;
logic SRC_pipo_en_aux;
logic DEST_pipo_en_aux;
logic FIFO_data_push_aux;
logic clr_interrupt_aux;
logic process_en_aux;
logic store_ctr_ovf;
logic data_pipo_en_aux;
logic store_ctr_en;
logic fifo_flush_aux;
logic rtx;
logic counter_en;
logic counter_ovf;
logic process_start;
logic FIFO_ctl_push_aux;
logic FIFO_ctl_pop_aux;
logic [7:0] all_fifos_empty_vec;
logic [7:0] all_counter_ovf_vector;
logic proc_rdy;
logic proc;
logic ctl_pop;
logic data_pop;
logic counter;
logic rtx_done;

assign L_pipo_en = L_pipo_en_aux;
assign CMD_pipo_en = CMD_pipo_en_aux;
assign M_pipo_en = M_pipo_en_aux;
assign SRC_pipo_en = SRC_pipo_en_aux;
assign data_pipo_en = data_pipo_en_aux;
assign DEST_pipo_en = DEST_pipo_en_aux;
assign FIFO_data_push = FIFO_data_push_aux;
assign clr_interrupt = clr_interrupt_aux;
assign process_en = process_en_aux;
assign fifo_flush = fifo_flush_aux;
assign rtx_en = rtx;
assign package_size = L_reg - 8'd4;
assign FIFO_ctl_push = FIFO_ctl_push_aux;
assign FIFO_ctl_pop = process_start;
//TODO: Fix signals below
assign req_cntr_en = counter_en;
assign rtx_done = 1'b1;


always_ff @( posedge uclk, negedge rst_n ) begin : cmds_states_fsm
    if (~rst_n)begin
        cmds_state <= qsys_pkg::IDLE;
	end
    else
        case(cmds_state)
            qsys_pkg::IDLE:   begin
                if (UART_data == CMD_START && rx_interrupt)
                    cmds_state <= START;
                else
                    cmds_state <= qsys_pkg::IDLE;
            end

            START:  begin
                if (rx_interrupt)
                    cmds_state <= CMD;
                else 
                    cmds_state <= START;
            end
            
            CMD:    begin
                if (rx_interrupt) begin
                    case(UART_data)
                        M_CONFIG_cmd: cmds_state <= M_CONFIG;
                        RTX_cmd: cmds_state <= RTX;
                        CAPTURE_cmd: cmds_state <= CAPTURE;
                        LOAD_cmd: cmds_state <= LOAD;
                        PROCESS_cmd: cmds_state <= PROCESS;
                        default: cmds_state <= CMD;
                    endcase
                end
                else 
                    cmds_state <= CMD;
            end
            
            M_CONFIG:   begin
                if (rx_interrupt)
                    cmds_state <= END;
                else   
                    cmds_state <= M_CONFIG;
            end

            RTX:    begin
                if (rtx_done)
                    cmds_state <= END;
                else 
                    cmds_state <= RTX;
            end
            CAPTURE:    cmds_state <= END;
            
            LOAD:   begin
                if (rx_interrupt)
                    cmds_state <= SOURCE;
                else 
                    cmds_state <= LOAD;
            end

            SOURCE: begin
                if (rx_interrupt)
                    cmds_state <= DEST;
                else
                    cmds_state <= SOURCE;
            end

            DEST:   begin
                if (rx_interrupt)
                    cmds_state <= STORE;
                else 
                    cmds_state <= DEST;
            end
            STORE:  begin
                if (store_ctr_ovf)
                    cmds_state <= END;
                else 
                    cmds_state <= STORE;
            end

            PROCESS:    begin
                if (proc_rdy)
                    cmds_state <= END;
                else
                    cmds_state <= PROCESS;
            end

            END:    begin
                if (UART_data == CMD_STOP)
                    cmds_state <= qsys_pkg::IDLE;
                else
                    cmds_state <= END;
            end
        endcase
end


always_ff @( posedge cclk, negedge rst_n ) begin : rtx_fsm
    if (~rst_n)
        rtx_aux <= wcrra_pkg::FALSE;
    else 
        case(cmds_state)
                qsys_pkg::IDLE: rtx_aux <= rtx_aux;
                START:  rtx_aux <= wcrra_pkg::FALSE;                
                CMD: rtx_aux = (rx_interrupt && UART_data == RTX_cmd)? wcrra_pkg::TRUE : rtx_aux;                
                M_CONFIG:  rtx_aux <= rtx_aux;
                RTX:  rtx_aux <= wcrra_pkg::FALSE;
                CAPTURE:    rtx_aux <= rtx_aux;                
                LOAD:  rtx_aux <= rtx_aux;
                SOURCE: rtx_aux <= rtx_aux;
                DEST:   rtx_aux <= rtx_aux;                
                STORE:  rtx_aux <= rtx_aux;
                PROCESS:   rtx_aux <= rtx_aux;
                END:    rtx_aux <= rtx_aux;
            endcase
end


always_comb begin : cmds_signals_fsm
    if (~rst_n)begin
       L_pipo_en_aux = wcrra_pkg::FALSE;
        CMD_pipo_en_aux = wcrra_pkg::FALSE;
        M_pipo_en_aux = wcrra_pkg::FALSE;
        SRC_pipo_en_aux = wcrra_pkg::FALSE;
        DEST_pipo_en_aux = wcrra_pkg::FALSE;
        FIFO_data_push_aux = wcrra_pkg::FALSE;
        clr_interrupt_aux = wcrra_pkg::FALSE;
        process_en_aux = wcrra_pkg::FALSE;
        rtx = wcrra_pkg::FALSE;
        data_pipo_en_aux = wcrra_pkg::FALSE;
        store_ctr_en = wcrra_pkg::FALSE;
        process_start = wcrra_pkg::FALSE;
        fifo_flush_aux = wcrra_pkg::FALSE;
        FIFO_ctl_push_aux = wcrra_pkg::FALSE;
	end

    else
        case(cmds_state)
            qsys_pkg::IDLE:   begin
                 L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;

                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end

            START:  begin
              L_pipo_en_aux = wcrra_pkg::TRUE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;

                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end
            
            CMD:    begin
               L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::TRUE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end
            
            M_CONFIG:   begin
                L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::TRUE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                capture_en_aux = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end

            RTX:    begin
                L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::TRUE;
                store_ctr_en = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end

            CAPTURE:    begin
                 L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::TRUE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end
            //TO fix this and beyond
            LOAD:   begin
                L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::TRUE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end

            SOURCE: begin
                L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::TRUE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end

            DEST:   begin
               L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux =(rx_interrupt)? wcrra_pkg::TRUE : wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::TRUE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                if (rx_interrupt) begin
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                    FIFO_ctl_push_aux = wcrra_pkg::TRUE;

                end
                else begin
                    clr_interrupt_aux = wcrra_pkg::FALSE;
                    FIFO_ctl_push_aux = wcrra_pkg::FALSE;

                end
            end
            STORE:  begin
              L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::TRUE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::TRUE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                clr_interrupt_aux = (rx_interrupt)? wcrra_pkg::TRUE : wcrra_pkg::FALSE;
                FIFO_data_push_aux =(rx_interrupt)? wcrra_pkg::TRUE : wcrra_pkg::FALSE;                    
            end

            PROCESS:    begin
                 L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::TRUE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::TRUE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end

            END:    begin
                 L_pipo_en_aux = wcrra_pkg::FALSE;
                CMD_pipo_en_aux = wcrra_pkg::FALSE;
                M_pipo_en_aux = wcrra_pkg::FALSE;
                SRC_pipo_en_aux = wcrra_pkg::FALSE;
                DEST_pipo_en_aux = wcrra_pkg::FALSE;
                FIFO_data_push_aux = wcrra_pkg::FALSE;
                data_pipo_en_aux = wcrra_pkg::FALSE;
                process_en_aux = wcrra_pkg::FALSE;
                rtx = wcrra_pkg::FALSE;
                store_ctr_en = wcrra_pkg::FALSE;
                fifo_flush_aux = wcrra_pkg::FALSE;
                FIFO_ctl_push_aux = wcrra_pkg::FALSE;
                process_start = wcrra_pkg::FALSE;
                if (rx_interrupt)
                    clr_interrupt_aux = wcrra_pkg::TRUE;
                else 
                    clr_interrupt_aux = wcrra_pkg::FALSE;
            end
        endcase
    end

cntr_mod_n_ovf_reconfig #( 
    .DW(8)
)store_counter(
    .clk(rx_interrupt),
    .rst_n(rst_n),
    .maxcnt(package_size+8'd1),
    .enb(store_ctr_en),
    .ovf(store_ctr_ovf),
    .count(count)
);

always_ff @( posedge cclk, negedge rst_n ) begin : wcrra_state_fsm
    if (~rst_n) 
        wcrra_state <= IDLE_PROC;
    else
        case (wcrra_state)
            IDLE_PROC: begin
                if (process_start)
                    wcrra_state <= PROC;
                else
                    wcrra_state <= IDLE_PROC;
            end 
            PROC: begin
                if (proc_rdy)
                    wcrra_state <= IDLE_PROC;
                else
                    wcrra_state <= PROC;
            end 
            default:
                wcrra_state <= IDLE_PROC;
        endcase
end




always_comb begin : wcrra_signals_fsm
    if (~rst_n) begin
        counter_en = wcrra_pkg::FALSE;
        proc_rdy = wcrra_pkg::FALSE;
    end
    else
        case (wcrra_state)
            IDLE_PROC: begin
                counter_en = wcrra_pkg::FALSE;
                proc_rdy = wcrra_pkg::FALSE;
            end

            PROC: begin
                counter_en = wcrra_pkg::TRUE;
                proc_rdy = (all_fifos_empty_vec == 8'hFF) ? wcrra_pkg::TRUE : wcrra_pkg::FALSE;
            end  
            default: begin
                counter_en = wcrra_pkg::FALSE;
                proc_rdy = wcrra_pkg::FALSE;
            end     
        endcase

    
end

endmodule
