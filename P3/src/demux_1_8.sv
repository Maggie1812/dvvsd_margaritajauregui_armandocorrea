`ifndef DEMUX_1_SV
    `define DEMUX_1_8_SV

module demux_1_8#(
    parameter DW = 4
)
(
    input logic [DW-1:0] in,
    input logic [2:0] sel,
  /*  output logic [DW-1:0] out_a,
    output logic [DW-1:0] out_b,
    output logic [DW-1:0] out_c,
    output logic [DW-1:0] out_d,
    output logic [DW-1:0] out_e,
    output logic [DW-1:0] out_f,
    output logic [DW-1:0] out_g,
    output logic [DW-1:0] out_h*/
    output logic [(8*DW)-1:0] out
);

always_comb begin 
    case (sel)
        3'b000: begin
            out[DW-1:0] = in;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end 
        3'b001: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = in;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end
        3'b010: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = in;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end 
        3'b011: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = in;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end
        3'b100: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = in;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end
        3'b101: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = in;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end 
        3'b110: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = in;
            out[8*DW-1:7*DW] = '0;
        end    
        3'b111: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = in;
        end 
        default: begin
            out[DW-1:0] = '0;
            out[2*DW-1:DW] = '0;
            out[3*DW-1:2*DW] = '0;
            out[4*DW-1:3*DW] = '0;
            out[5*DW-1:4*DW] = '0;
            out[6*DW-1:5*DW] = '0;
            out[7*DW-1:6*DW] = '0;
            out[8*DW-1:7*DW] = '0;
        end    
    endcase 
end 

endmodule
`endif
