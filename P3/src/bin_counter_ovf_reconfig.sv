module bin_counter_ovf_reconfig #(
parameter 
DW      = 4
)(
input           clk,
input           rst_n,
input           enb,
input  logic [DW-1:0] maxcnt,
output logic    ovf,
output  logic [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;


always_ff@(posedge clk, negedge rst_n)begin: counter
    if (!rst_n)
        count_r     <=  '0          ;
    else if (enb)
        count_r     <= count_nxt    ;
    else 
        count_r     <= '0           ;
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
   count_nxt = count_r + 1'b1 ;
    if (count_r == maxcnt-1'b1)
        ovf     =   1'b1;    
    else
        ovf     =   1'b0;
end

assign count    =   count_r;

endmodule
