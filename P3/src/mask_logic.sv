import wcrra_pkg::*;

module mask_logic(
    input data_t request_vector,
    input data_t mask_vector,
    output data_t masked_request_vector 
);

assign masked_request_vector = mask_vector & request_vector;

endmodule