

module ppc #(
    parameter DW = 5
)
(
    input logic [DW-1:0] request_vector,
    output logic [DW-1:0] ppc_output
);

logic [DW-1:0] ppc_aux;


assign ppc_aux[0] = request_vector[0];


genvar i;
generate for( i =1;i<DW; i++) begin:or_
    or_mod o(
        .in_a(ppc_aux[i-1]),
        .in_b(request_vector[i]),
        .out(ppc_aux[i])
    );

 end endgenerate



assign ppc_output  = ppc_aux;
endmodule