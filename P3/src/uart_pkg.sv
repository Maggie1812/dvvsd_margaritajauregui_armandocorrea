`ifndef UART_PKG_SV
    `define UART_PKG_SV
    package uart_pkg;

    localparam DATA_SIZE = 8;
    localparam START_BIT = 1'b0;
    localparam STOP_BIT = 1'b1;
    localparam TRUE = 1'b1;
    localparam FALSE = 1'b0;
    localparam UART_PKG_SIZE = 11;
    localparam BYTE_SIZE = 8;
    typedef logic [BYTE_SIZE-1:0]   byte_t;
    typedef logic flag_t;
    typedef enum logic [1:0] { 
        IDLE =  2'b00,
        TRANSMIT = 2'b01,
        RECEIVE = 2'b10,
        HOLD = 2'b11
    } fsm_uart_state_e;

typedef struct {
    logic start_bit;
    byte_t      data;
    logic  parity_bit;
    logic stop_bit;
} TX_PKG;
    endpackage

`endif 
