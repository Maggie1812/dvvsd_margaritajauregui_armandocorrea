import wcrra_pkg::data_t;
import wcrra_pkg::dwb2log_data_t;
module wcrra #(
    parameter DW = wcrra_pkg::DW
)
(
    input bit clk,
    input bit rst_n,
    input data_t request_vector,
    input logic ack,
    output logic valid,
    output dwb2log_data_t grant,
    output data_t grant_one_shot
);

logic enable_w;
logic valid_w;
logic masked_value;
logic ack_oneshot;
logic rst_n_oneshot;
data_t masked_request_vector;
data_t mask_vector;
data_t mask_vector_reg;
data_t ppc_input_vector;
data_t ppc_output_w;

    assign ack_oneshot = ack;
    assign grant_one_shot = ppc_input_vector;


///////////////////////////////////////////////////////////////////////////////
//
//          Control Unit
//
///////////////////////////////////////////////////////////////////////////////


wcrra_control control(
    .clk(clk),
    .rst_n(rst_n),
    .ack(ack_oneshot),
    .valid(valid_w)
);
assign valid = valid_w;
///////////////////////////////////////////////////////////////////////////////
//
//          Mask logic Unit
//
///////////////////////////////////////////////////////////////////////////////


mask_logic mask_logic_unit(
    .request_vector(request_vector),
    .mask_vector(mask_vector_reg),
    .masked_request_vector(masked_request_vector)
);

assign masked_value = (masked_request_vector == '0)? 1'b1 : 1'b0;

mux_2_1 #(.DW(DW))
mask_vector_mux(
    .in_a(ppc_output_w),
    .in_b({ppc_output_w[DW-2:0], 1'b0}),
    .sel(ack_oneshot),
    .out(mask_vector)
);

pipo #(.DW(DW))
mask_next_pipo (
    .clk(clk),
    .rst_n(rst_n),
    .en(rst_n),
    .dataIn(mask_vector),
    .dataOut(mask_vector_reg)
);
///////////////////////////////////////////////////////////////////////////////
//
//          PPC
//
///////////////////////////////////////////////////////////////////////////////


mux_2_1 #(.DW(DW))
ppc_input_mux(
    .in_a(masked_request_vector),
    .in_b(request_vector),
    .sel(masked_value),
    .out(ppc_input_vector)
);

ppc #(.DW(DW))
ppc_unit(
    .request_vector(ppc_input_vector),
    .ppc_output(ppc_output_w)
);



///////////////////////////////////////////////////////////////////////////////
//
//         Binary decoder
//
///////////////////////////////////////////////////////////////////////////////


binary_decoding #(.DW(DW))
b_decoding(
    .ppc_output(ppc_output_w),
    .binary_decoded_index(grant)
);




endmodule
