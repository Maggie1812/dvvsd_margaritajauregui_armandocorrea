import wcrra_pkg::*;

module binary_decoding #(
    parameter DW = 4
)
(
    input data_t ppc_output,
    output dwb2log_data_t binary_decoded_index
);
dwb2log_data_t index;
logic found;
integer i;

always_comb begin
    found = 0;
    index = 0;
     for( i =0;i<DW; i++) begin
        if (ppc_output[i] == wcrra_pkg::TRUE && found == wcrra_pkg::FALSE) begin
            found = wcrra_pkg::TRUE;
            index = i;
        end

 end 

end

assign binary_decoded_index  = index;
endmodule
