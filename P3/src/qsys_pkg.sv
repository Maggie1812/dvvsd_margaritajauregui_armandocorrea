`ifndef QSYS_PKG_SV
    `define QSYS_PKG_SV
package qsys_pkg;

localparam CMD_START = 'hFE;
localparam CMD_STOP = 'hEF;
localparam M_CONFIG_cmd = 'h01;
localparam RTX_cmd = 'h02;
localparam CAPTURE_cmd = 'h03;
localparam LOAD_cmd = 'h04;
localparam PROCESS_cmd = 'h05;
localparam L_SIZE = 5;
localparam DEST_SIZE = 3;

typedef enum logic [3:0]{
    IDLE = 4'b0000, 
    START = 4'b0001, 
    CMD = 4'b0010, 
    M_CONFIG = 4'b0011,
    RTX = 4'b0100,
    CAPTURE = 4'b0101,
    LOAD = 4'b0111,
    SOURCE = 4'b1000,
    DEST = 4'b1001,
    STORE = 4'b1010,
    END = 4'b1011,
    PROCESS = 4'b1100

} fsm_cmds_state_e;


typedef enum logic [2:0]{
    PROCESS_IDLE = 3'b000, 
    CTL_POP = 3'b001, 
    DATA_POP = 3'b010, 
    START_WCRRA = 3'b011,
    WCRRA_CYCLE = 3'b100,
    MOVE = 3'b101,
    STOP_WCRRA = 3'b111
} fsm_wcrra_state_e;

typedef enum logic { 
    IDLE_PROC = 1'b0,
    PROC = 1'b1
} fsm_wcrra_states_e;


endpackage
`endif 

