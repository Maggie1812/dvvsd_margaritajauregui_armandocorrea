`ifndef FIFO
    `define FIFO
import sdp_dc_ram_pkg::*;

module fifo #(
    DATA_WIDTH=W_DATA, 
    BUFFER_DEPTH=W_DEPTH
  )(
    input  logic                    wr_clk,   //clk
    input  logic                    rd_clk,   //clk
    input  logic                    rst,   //rst
    input  logic                    push,  //push enable
    input  logic                    pop,  // pop enable
    input  bit                      flush,
    input  data_t                   DataInput,    //data input
    output data_t                   DataOutput,   //data output
    output logic                    empty,  // signal of empty
    output logic                    full    //signal of full
  );
    
  logic nempty;
  logic nfull;
  logic nempty_next;
  logic nfull_next;
  logic we_next;
  bit one_element;
  bit one_element_pop;
  data_t DataOutput_next;
   
  logic [W_ADDR:0] wr_ptr, nwr_ptr;           // Write pointer
   addr_t rd_ptr, nrd_ptr;           // Read pointer

  sdp_dc_ram_if mem_interface();
  
  sdp_dc_ram ram_instance(
    .clk_a(wr_clk),
    .clk_b(rd_clk),
    .mem_if(mem_interface.mem)
  );
 // --------------------------------------------
 // --------------------------------------------
 // --------------READ------------------------------
 // --------------------------------------------
 // --------------------------------------------
  always_ff @(posedge rd_clk or negedge rst) begin 
    if(!rst ) begin
         rd_ptr <= '0;
        empty  <= '1;
        one_element <= 1'b0;
    end else if (~flush) begin //if fifo isnt full data input is saved in the buffer
      empty   <= nempty;
        rd_ptr <= (nrd_ptr != wr_ptr) ?  nrd_ptr : rd_ptr;
        one_element <= (nrd_ptr == wr_ptr |  one_element_pop) ? 1'b0 : 1'b1;
    end
else begin
        rd_ptr <= '0;
        empty  <= '1;
    end
  end
//----------------POINTERS------------------------

assign nrd_ptr  = (pop && !empty ) ? rd_ptr+1'b1 : rd_ptr;

// ----------------DATA OUT---------------------

  assign DataOutput = DataOutput_next;

  always_ff @(posedge rd_clk or negedge rst) begin
    if(!rst)  begin
      mem_interface.rd_addr_b <= 0;
      DataOutput_next <= 'd0 ;
      one_element_pop <= 1'b0;
    end else if (~flush) begin //if fifo isnt full data input is saved in the buffer
      mem_interface.rd_addr_b <= (pop && !empty) ? rd_ptr : mem_interface.rd_addr_b;
      DataOutput_next <= (pop && !empty) ? mem_interface.rd_data_a : (!one_element_pop) ? 1'b0 : DataOutput ;
        one_element_pop <= (pop && one_element) ?  1'b1: (one_element_pop) ? 1'b1 : 1'b0 ;
    end else begin 
      mem_interface.rd_addr_b <= 0;
      DataOutput_next <= 'd0 ;
      one_element_pop <= 1'b0;
    end
end
  
      

// --------------------------------------------
 // --------------------------------------------
 // --------------WRITE------------------------------
 // --------------------------------------------
 // --------------------------------------------
  always_ff @(posedge wr_clk or negedge rst) begin 
    if(!rst) begin
         wr_ptr <= '0;
        full   <= '0;
    end
    else if(~flush) begin
        full    <= !nfull;
        wr_ptr <= nwr_ptr; 
        end
    else begin
         wr_ptr <= '0;
        full   <= '0;
    end 
  end

       assign nwr_ptr  = (push && !full) ? wr_ptr+1'b1 : (!flush) ? wr_ptr : '0 ;
       assign we_next = (push && !full && rst) ? '1:'0;  

      assign mem_interface.we_a      = we_next;


always_ff @(posedge wr_clk or negedge rst) begin
    if(!rst) begin
      mem_interface.data_a    <= '0;
      mem_interface.wr_addr_a <= 0;
    end else if (~flush) begin
      mem_interface.data_a    <= DataInput;
      mem_interface.wr_addr_a <=  wr_ptr[W_ADDR-1:0];
    end else begin //if fifo isnt full data input is saved in the buffer
      mem_interface.data_a    <= '0;
      mem_interface.wr_addr_a <= 0;
    end
end
  
 //-------------------------------------------------------------------
               
  always_comb begin
      nempty = (!rst | flush | one_element_pop) ? '1 : (wr_ptr == {W_ADDR{1'b0}}) ? 1'b1 : 1'b0;
    nfull =  (!rst | flush) ? '1 : (wr_ptr[W_ADDR] == 1'b1) ? 1'b0: 1'b1; 
  end          




endmodule: fifo

`endif
