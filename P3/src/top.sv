import uart_pkg::byte_t;
import qsys_pkg::*;
import clk_div_pkg::*;

module top(
    input bit clk,
    input bit rst_n,
    input byte_t data_to_transmit,
    input logic transmit,
    input logic serial_data_rx
);
localparam BAUDRATE = UART_BAUDRATE_19200;
bit uclk;
bit cclk;
byte_t uart_data_received;
logic rx_interrupt;
logic clr_interrupt;
logic serial_output_tx;
logic parity_error;
logic baudrate_gen_ovf;





`ifdef SIMULATION
assign cclk = clk;
`else
clock_generator clk_gen
(
    .rst_n(rst_n),
    .clk(clk),
    .clk_div(cclk)
);
`endif


baudrate_generator#(
    .BAUDRATE(25000000),
    .FREQUENCY_REF(FREQUENCY_CLK_FPGA) //50_000_000 
)
baudrate_gen(
    .rst_n(rst_n),
    .clk(clk),
    .clk_div(uclk)
);

//-------------------------------UART---------------------------------
uart UART (
	.clk(uclk),
	.rst_n(rst_n),
	.serial_data_rx(serial_data_rx),
	.serial_output_tx(serial_output_tx),
	.rx_interrupt(rx_interrupt), //flag for data received
	.clr_interrupt(clr_interrupt),
	.transmit(transmit), //start tx
	.data_to_transmit(data_to_transmit), //data to transmit
	.data_received(uart_data_received), //data received
    .parity_error(parity_error)
);



qsys QSYS(
    .cclk(cclk),
    .uclk(uclk), 
    .rst_n(rst_n),
    .dataIn(uart_data_received),
    .rx_interrupt(rx_interrupt),
    .clr_uart_interrupt(clr_interrupt),
    .dataOut()
);



endmodule
