
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow

module bin_counter_ovf #(
parameter 
DW      = 4, 
MAXCNT  = 5
)(
input           clk,
input           rst_n,
input           enb,
output logic    ovf,
output [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;


always_ff@(posedge clk, negedge rst_n)begin: counter
    if (!rst_n)
        count_r     <=  '0          ;
    else if (enb)
        count_r     <= count_nxt    ;
    else 
        count_r     <= '0           ;
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
   count_nxt = count_r + 1'b1 ;
    if (count_r == MAXCNT-1'b1)
        ovf     =   1'b1;    
    else
        ovf     =   1'b0;
end

assign count    =   count_r;

endmodule
