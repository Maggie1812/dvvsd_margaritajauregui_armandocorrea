import uart_pkg::byte_t;
import uart_pkg::BYTE_SIZE;
import wcrra_pkg::*;
import qsys_pkg::*;
module qsys
    #(parameter M = 8)
(
    input bit cclk,
    input bit uclk, 
    input bit rst_n,
    input byte_t dataIn,
    input logic rx_interrupt,
    output logic clr_uart_interrupt,
    output byte_t dataOut
);
localparam N = 8;

byte L_reg;


logic [N-1:0] ack_vector;
logic [N-1:0] [DW_B2LOG-1:0] grant_vector_array;
logic [(BYTE_SIZE*N)-1:0] request_vector_array;
logic [BYTE_SIZE-1:0] FIFO_ctl_push_vector;
logic [BYTE_SIZE-1:0] FIFO_data_push_vector;
logic [BYTE_SIZE-1:0] FIFO_data_pop_vector;
byte l_reg;
byte cmd_reg;
byte src_reg;
byte dest_reg;
byte m_reg;
byte dataIn_reg;
logic [BYTE_SIZE-1:0] ififo_push_select_w;
logic clr_interrupt;
logic fifo_flush;
logic process_en;
logic l_pipo_en;
logic m_pipo_en;
logic cmd_pipo_en;
logic src_pipo_en;
logic dest_pipo_en;
logic FIFO_data_push;
logic FIFO_ctl_push;
logic rtx_en;
logic data_push_w;
logic control_push_w;
logic [BYTE_SIZE*8-1:0] data_out_v;
logic [BYTE_SIZE*8-1:0]  ctl_out_v;
logic [BYTE_SIZE*8-1:0] ctl_out_reg_v;
logic [DEST_SIZE*8-1:0] dest_reg_v;
logic [L_SIZE*8-1:0] l_reg_v;
byte ctl_empty_v;
logic [7:0] data_empty_v;
byte ctl_full_v;
byte data_full_v;
logic FIFO_ctl_pop;
logic [7:0] req_ctr_en_v;
logic [7:0] req_ctr_ovf_v;
logic [BYTE_SIZE-1:0] FIFOs_out;
logic [BYTE_SIZE-1:0] FIFO_retro;
logic [N-1:0] [BYTE_SIZE-1:0] grant_mux_w;
logic [N-1:0] [BYTE_SIZE-1:0] oFIFOS_out;
logic [N-1:0] [BYTE_SIZE-1:0] oFIFOs_dataIn_v;
logic [N-1:0] [BYTE_SIZE-1:0] req_vector_array;
logic [N-1:0] [(8*BYTE_SIZE)-1:0] data_vector_array;
logic [N-1:0] [BYTE_SIZE-1:0] grant_one_shot;
logic [N-1:0] [BYTE_SIZE-1:0] empty_vector_v;
logic req_cntr_en;
logic data_pipo_en;
logic ctl_pop_pipo_en;
logic [N-1:0] [4:0] cnt;
logic [N-1:0] [4:0] cnt_aux;
assign clr_uart_interrupt = clr_interrupt;
assign data_push_w = 1'b1 && FIFO_data_push;
assign control_push_w = 1'b1 && FIFO_ctl_push;
assign ififo_push_select_w = src_reg%m_reg;
assign ctl_pop_pipo_en = req_cntr_en;
//-------------------------------CONTROL-------------------------------
qsys_control qsys__control(
    .cclk(cclk),
    .uclk(uclk),
    .rst_n(rst_n),
    .UART_data(dataIn),
    .L_reg(l_reg),
    .rx_interrupt(rx_interrupt),
    .clr_interrupt(clr_interrupt),
    .fifo_flush(fifo_flush),
    .process_en(process_en),
    .L_pipo_en(l_pipo_en),
    .M_pipo_en(m_pipo_en),
    .CMD_pipo_en(cmd_pipo_en),
    .SRC_pipo_en(src_pipo_en),
    .DEST_pipo_en(dest_pipo_en),
    .data_pipo_en(data_pipo_en),
    .FIFO_data_push(FIFO_data_push),
    .FIFO_ctl_push(FIFO_ctl_push),
    .FIFO_ctl_pop(FIFO_ctl_pop),
    .req_cntr_en(req_cntr_en),
    .rtx_en(rtx_en)
);
// PIPOS

pipo #(.DW(8))
L_pipo(   
        .clk(uclk),
        .rst_n(rst_n),
        .en(l_pipo_en),
        .dataIn(dataIn),
        .dataOut(l_reg)
    );


pipo #(.DW(8))
DEST_pipo(   
        .clk(uclk),
        .rst_n(rst_n),
        .en(dest_pipo_en),
        .dataIn(dataIn),
        .dataOut(dest_reg)
    );

pipo #(.DW(8))
SRC_pipo(   
        .clk(uclk),
        .rst_n(rst_n),
        .en(src_pipo_en),
        .dataIn(dataIn),
        .dataOut(src_reg)
    );


pipo #(.DW(8))
CMD_pipo(   
        .clk(uclk),
        .rst_n(rst_n),
        .en(cmd_pipo_en),
        .dataIn(dataIn),
        .dataOut(cmd_reg)
    );


pipo #(.DW(8))
M_pipo(   
        .clk(uclk),
        .rst_n(rst_n),
        .en(m_pipo_en),
        .dataIn(dataIn),
        .dataOut(m_reg)
    );

pipo #(.DW(8))
data_pipo(   
        .clk(uclk),
        .rst_n(rst_n),
        .en(data_pipo_en),
        .dataIn(dataIn),
        .dataOut(dataIn_reg)
    );



demux_1_8 #(.DW(1))
iFIFO_ctl_push_1_8_demux(
    .in(control_push_w),
    .sel(ififo_push_select_w[2:0]),
    .out(FIFO_ctl_push_vector)
);

demux_1_8 #(.DW(1))
iFIFO_data_push_1_8_demux(
    .in(data_push_w),
    .sel(ififo_push_select_w[2:0]) ,
    .out(FIFO_data_push_vector)
);


genvar i;
generate for(i=1; i<=8; i++) begin:iFIFOs_
    ififo_set iFIFO_set(
        .rd_clk(cclk), 
        .wr_clk(uclk),
        .rst_n(rst_n),
        .ctl_push(FIFO_ctl_push_vector[i-1] && ~ctl_full_v[i-1]),
        .ctl_pop(FIFO_ctl_pop || req_ctr_ovf_v[i-1]), 
        .data_push(FIFO_data_push_vector[i-1] && ~data_full_v[i-1]),
        .data_pop(FIFO_data_pop_vector[i-1]),
        .flush(fifo_flush),
        .ctl_dataIn({l_reg[4:0], dest_reg[2:0]}),
        .data_dataIn(dataIn_reg),
        .ctl_dataOut(ctl_out_v[(8*i)-1:8*(i-1)]),
        .data_dataOut(data_out_v[(8*i)-1:8*(i-1)]),
        .ctl_empty(empty_vector_v[i-1]),
        .data_empty(data_empty_v[i-1]),
        .ctl_full(ctl_full_v[i-1]),
        .data_full(data_full_v[i-1])
    );

    pipo #(.DW(8))
    ctl_out_pipo(   
        .clk(cclk),
        .rst_n(rst_n),
        .en(ctl_pop_pipo_en),
        .dataIn(ctl_out_v[(8*i)-1:8*(i-1)]),
        .dataOut(ctl_out_reg_v[(8*i)-1:8*(i-1)])
    );
    assign dest_reg_v[(DEST_SIZE*i)-1:DEST_SIZE*(i-1)] = ctl_out_reg_v[(8*i)-(L_SIZE+1):8*(i-1)];
    assign l_reg_v[(L_SIZE*i)-1:L_SIZE*(i-1)] = ctl_out_reg_v[(8*i)-1:(8*i)-L_SIZE];
    assign req_ctr_en_v[i-1] = req_cntr_en && grant_mux_w[i-1];
    
    bin_counter_ovf_reconfig #(.DW(5))
    request_counter(
        .clk(cclk),
        .rst_n(rst_n),
        .maxcnt(l_reg_v[(L_SIZE*i)-1:L_SIZE*(i-1)]),
        .enb(req_ctr_en_v[i-1]),
        .ovf(req_ctr_ovf_v[i-1]),
        .count(cnt[i-1])

    );

    demux_1_8 #(.DW(1))
    req_vector_demux(
        .in(~empty_vector_v[i-1]),
        .sel(dest_reg_v[((BYTE_SIZE*i)-L_SIZE)-1:BYTE_SIZE*(i-1)]) ,
        .out(request_vector_array[i-1])
    );


assign ack_vector[i-1] = req_ctr_ovf_v[i-1];


    wcrra #(.DW(N)) 
    WCRRA(
        .clk(cclk),
        .rst_n(rst_n),
        .request_vector(request_vector_array[(N*i)-1:N*(i-1)]),
        .ack(ack_vector[i-1]),
        .grant(grant_vector_array[i-1]),
        .valid(),
        .grant_one_shot(grant_one_shot[(N*i)-1:N*(i-1)])
    );


assign FIFO_data_pop_vector[i-1] = grant_mux_w[i-1] && ~req_ctr_ovf_v[i-1] && req_ctr_en_v[i-1];
    always_ff@(posedge cclk) begin 
        cnt_aux[i-1] <= (!rst_n) ? '0 : cnt[i-1];
    end


 demux_1_8 #(.DW(BYTE_SIZE))
    data_vector_demux(
    .in(data_out_v[(BYTE_SIZE*i)-1:BYTE_SIZE*(i-1)]),
    .sel(dest_reg_v[((BYTE_SIZE*i)-L_SIZE)-1:BYTE_SIZE*(i-1)]) ,
    .out(data_vector_array[i-1])
);




mux_8_1#(
    .DW(BYTE_SIZE)
) oFIFOs_mux(
    .in_a(data_vector_array[0][(8*BYTE_SIZE)-1:7*BYTE_SIZE]),
    .in_b(data_vector_array[1][(7*BYTE_SIZE)-1:6*BYTE_SIZE]),
    .in_c(data_vector_array[2][(6*BYTE_SIZE)-1:5*BYTE_SIZE]),
    .in_d(data_vector_array[3][(5*BYTE_SIZE)-1:4*BYTE_SIZE]),
    .in_e(data_vector_array[4][(4*BYTE_SIZE)-1:3*BYTE_SIZE]),
    .in_f(data_vector_array[5][(3*BYTE_SIZE)-1:2*BYTE_SIZE]),
    .in_g(data_vector_array[6][(2*BYTE_SIZE)-1:1*BYTE_SIZE]),
    .in_h(data_vector_array[7][(BYTE_SIZE)-1:0]),
    .sel(grant_vector_array[i-1]),
    .out(oFIFOs_dataIn_v[i-1])
);


    fifo #(
    .DATA_WIDTH(BYTE_SIZE),
    .BUFFER_DEPTH(16)
)oFIFO_set(
        .wr_clk(cclk),  
        .rd_clk(cclk),
        .rst(rst_n),
        .push(),
        .pop(),
        .flush(),
        .DataInput(oFIFOs_dataIn_v[i-1]), 
        .DataOutput(oFIFOS_out[i-1]),
        .empty(),
        .full()
    );

mux_8_1#(
    .DW(BYTE_SIZE)
) Grant_MUX(
    .in_a(grant_one_shot[0]),
    .in_b(grant_one_shot[1]),
    .in_c(grant_one_shot[2]),
    .in_d(grant_one_shot[3]),
    .in_e(grant_one_shot[4]),
    .in_f(grant_one_shot[5]),
    .in_g(grant_one_shot[6]),
    .in_h(grant_one_shot[7]),
    .sel(dest_reg_v[((BYTE_SIZE*i)-L_SIZE)-1:BYTE_SIZE*(i-1)]),
    .out(grant_mux_w[i-1])
);

end



endgenerate

mux_8_1 #(
    .DW(BYTE_SIZE)
)oFIFOs_mux_out(
    .in_a(oFIFOS_out[0]),
    .in_b(oFIFOS_out[1]),
    .in_c(oFIFOS_out[2]),
    .in_d(oFIFOS_out[3]),
    .in_e(oFIFOS_out[4]),
    .in_f(oFIFOS_out[5]),
    .in_g(oFIFOS_out[6]),
    .in_h(oFIFOS_out[7]),
    .sel(),
    .out(FIFOs_out)
);

mux_2_1 #(
    .DW(BYTE_SIZE)
)OFIFO_mux(
    .in_a(FIFOs_out),
    .in_b(dataOut),
    .sel(1'b1),
    .out(FIFO_retro)
);

fifo broadcast(
 .wr_clk(cclk),  
 .rd_clk(cclk),
 .rst(rst_n),
 .push(),
 .pop(),
 .flush(),
 .DataInput(FIFO_retro), 
 .DataOutput(dataOut),
 .empty(),
 .full()
);

endmodule
