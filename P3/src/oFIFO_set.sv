import uart_pkg::*;

module ofifo_set(
    input bit rd_clk, 
    input bit wr_clk,
    input bit rst_n,
    input logic data_push,
    input logic data_pop, 
    input logic rtx_pop,
    input byte data_dataIn,
    output byte dataOut
);


byte data_dataOut;
byte rtx_dataOut;
logic data_empty;
logic data_full;
logic rtx_empty;
logic rtx_full;

fifo #(
    .DATA_WIDTH(BYTE_SIZE), 
    .BUFFER_DEPTH(16)
)data_ofifo(
    .wr_clk(wr_clk),   
    .rd_clk(rd_clk),   
    .rst(rst_n),   
    .push(data_push),  
    .pop(data_pop),  
    .DataInput(data_dataIn),
    .DataOutput(data_dataOut),
    .empty(data_empty),
    .full(data_full)  
  );


fifo #(
    .DATA_WIDTH(BYTE_SIZE), 
    .BUFFER_DEPTH(16)
)rtx_ofifo(
    .wr_clk(rd_clk),   
    .rd_clk(rd_clk),   
    .rst(rst_n),   
    .push(data_pop),  
    .pop(rtx_pop),  
    .DataInput(data_dataOut),
    .DataOutput(data_dataOut),
    .empty(rtx_empty),
    .full(rtx_empty)  
  );
endmodule
