`ifndef TB_PKG
    `define TB_PKG

package tb_qsys_pkg;
    
    localparam DW = 8;
    localparam UPERIOD = 4;
    localparam CPERIOD = 2;

    typedef logic [DW-1:0] byte_t;
endpackage
`endif
