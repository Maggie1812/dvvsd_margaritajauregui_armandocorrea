`timescale 10ns / 1ps
`include "tester.svh"
import uart_pkg::*;
import qsys_pkg::*;

module qsys_tb();


bit clk;
bit rst_n;

    tester t;

    tb_qsys_if itf(
        .cclk(clk)
    );

top uut(
    .clk(clk),
    .rst_n(rst_n),
    .data_to_transmit(itf.parallel_data_tx),
    .transmit(itf.transmit),
	.serial_data_rx(itf.serial_data_rx)
);

initial
begin
    t = new(itf);
	/** Begins test*/
	rst_n = 1'b1;
	clk   = 1'b0;
	#1 rst_n = 1'b0;
	#2 rst_n = 1'b1;
    t.init();
fork
    t.send_full_routine();
join

    $stop();
end

always begin
    #1 clk <= ~clk;
end




endmodule
