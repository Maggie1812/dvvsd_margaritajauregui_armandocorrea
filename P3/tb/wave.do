onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label sys_clk /qsys_tb/uut/clk
add wave -noupdate -label uclk /qsys_tb/uut/uclk
add wave -noupdate /qsys_tb/rst_n
add wave -noupdate -label cclk /qsys_tb/uut/cclk
add wave -noupdate /qsys_tb/uut/rst_n
add wave -noupdate -label QSYS_data2transmot -radix hexadecimal /qsys_tb/uut/data_to_transmit
add wave -noupdate -label transmit /qsys_tb/uut/transmit
add wave -noupdate -label rx_interrupt /qsys_tb/uut/rx_interrupt
add wave -noupdate -label UART_dataReceived -radix hexadecimal /qsys_tb/uut/uart_data_received
add wave -noupdate -label clrInterrupt /qsys_tb/uut/clr_interrupt
add wave -noupdate -divider PIPOs
add wave -noupdate -label L_PIPO -radix hexadecimal /qsys_tb/uut/QSYS/L_pipo/dataOut
add wave -noupdate -label CMD_PIPO -radix hexadecimal /qsys_tb/uut/QSYS/CMD_pipo/dataOut
add wave -noupdate -label M_PIPO -radix hexadecimal /qsys_tb/uut/QSYS/M_pipo/dataOut
add wave -noupdate -label DEST_PIPO -radix hexadecimal /qsys_tb/uut/QSYS/DEST_pipo/dataOut
add wave -noupdate -label SRC_PIPO -radix hexadecimal /qsys_tb/uut/QSYS/SRC_pipo/dataOut
add wave -noupdate -divider iFIFOs
add wave -noupdate -color Yellow -label iFIFO_0_ctlPush {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/ctl_push}
add wave -noupdate -color Yellow -label iFIFO_0_ctlData -radix binary {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/ctl_dataIn}
add wave -noupdate -color Yellow -label iFIFO_0_ctlPop {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/control_fifo/pop}
add wave -noupdate -color Yellow -label iFIFO_0_ctlOut {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/control_fifo/DataOutput}
add wave -noupdate -color Yellow -label iFIFO_0_dataPush {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/data_push}
add wave -noupdate -color Yellow -label iFIFO_0_dataData -radix hexadecimal {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/data_dataIn}
add wave -noupdate -color Yellow -label iFIFO_0_dataPop {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/data_fifo/pop}
add wave -noupdate -color Yellow -label iFIFO_0_dataOut {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/data_fifo/DataOutput}
add wave -noupdate -color Yellow -label iFIFO_0_ctlEmpty {/qsys_tb/uut/QSYS/iFIFOs_[1]/iFIFO_set/ctl_empty}
add wave -noupdate -color Cyan -label iFIFO_1_ctlPush {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/ctl_push}
add wave -noupdate -color Cyan -label iFIFO_1_dataPush {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/data_push}
add wave -noupdate -color Cyan -label iFIFO_1_ctlData -radix binary {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/ctl_dataIn}
add wave -noupdate -color Cyan -label iFIFO_1_dataDataIn -radix hexadecimal {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/data_dataIn}
add wave -noupdate -color Cyan -label iFIFO_1_ctlPop {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/ctl_pop}
add wave -noupdate -color Cyan -label iFIFO_1_dataPop {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/data_pop}
add wave -noupdate -color Cyan -label iFIFO_1_ctlOut -radix binary {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/ctl_dataOut}
add wave -noupdate -color Cyan -label iFIFO_1_dataOut {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/data_dataOut}
add wave -noupdate -color Cyan -label iFIFO_1_ctlEmpty {/qsys_tb/uut/QSYS/iFIFOs_[2]/iFIFO_set/ctl_empty}
add wave -noupdate -divider QSYS
add wave -noupdate /qsys_tb/uut/QSYS/qsys__control/cmds_state
add wave -noupdate /qsys_tb/uut/QSYS/qsys__control/count
add wave -noupdate /qsys_tb/uut/QSYS/qsys__control/store_ctr_ovf
add wave -noupdate /qsys_tb/uut/QSYS/qsys__control/store_ctr_en
add wave -noupdate -radix hexadecimal /qsys_tb/uut/QSYS/dataIn
add wave -noupdate /qsys_tb/uut/QSYS/rx_interrupt
add wave -noupdate /qsys_tb/uut/QSYS/clr_uart_interrupt
add wave -noupdate /qsys_tb/uut/QSYS/process_en
add wave -noupdate /qsys_tb/uut/QSYS/l_pipo_en
add wave -noupdate /qsys_tb/uut/QSYS/m_pipo_en
add wave -noupdate /qsys_tb/uut/QSYS/cmd_pipo_en
add wave -noupdate /qsys_tb/uut/QSYS/src_pipo_en
add wave -noupdate /qsys_tb/uut/QSYS/dest_pipo_en
add wave -noupdate /qsys_tb/uut/QSYS/rtx_en
add wave -noupdate /qsys_tb/uut/QSYS/data_push_w
add wave -noupdate /qsys_tb/uut/QSYS/control_push_w
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {32310000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 452
configure wave -valuecolwidth 228
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {36676500 ps}
