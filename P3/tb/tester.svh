class tester;

    localparam DW = 8;

    localparam TRUE = 1'b1;
    localparam FALSE = 1'b0;

`protect
    typedef logic [DW-1:0] byte_t;
 localparam START = 'hFE;
localparam STOP = 'hEF;
localparam M_CONFIG = 'h01;
localparam RTX = 'h02;
localparam CAPTURE = 'h03;
localparam LOAD = 'h04;
localparam PROCESS = 'h05;
   localparam CPERIOD = 4;
`endprotect

    bit clk;
    bit rst_n;
    byte data;
    logic transmit;
    logic serial_data_rx;
    logic clr_interrupt;
    integer i;
    byte_t parallel_data_tx;

    byte_t q_brodcast[$];
    virtual tb_qsys_if itf;


    `protect
    function new(virtual tb_qsys_if.tstr t);
        itf = t;
    endfunction

task automatic send_rx_config(byte_t l = 8'h01,byte_t m = 8'h01);
	data_send_rx(START);
	data_send_rx(l);	//L
	data_send_rx(M_CONFIG);	//CMD = 1 -> Configurar M 
	data_send_rx(m);	//M
	data_send_rx(STOP);
endtask

task automatic send_rx_rtx_capture_procces(byte_t l = 8'h01,byte_t cmd = 8'h01);
	data_send_rx(START);
	data_send_rx(l);	//L
	data_send_rx(cmd);	//CMD = 2 -> Retransmitir
	data_send_rx(STOP);
endtask


task automatic send_rx_load(byte_t l = 8'h01, byte_t src = 8'h01, byte_t dest = 8'h01,integer buffer_depth = 'd0, byte_t [10] buffer_data );
	data_send_rx(START);
	data_send_rx(l);	//L
	data_send_rx(LOAD);	//CMD = 4 -> Cargar datos
	data_send_rx(src);	//SRC
	data_send_rx(dest);	//DEST
    for (i = 0; i<buffer_depth; i=i+1) begin
    	data_send_rx(buffer_data[i]);	//D0
    end
    data_send_rx(STOP);
endtask


//Byte to send with UART
task automatic data_send_rx(byte_t	uart_in = 8'h01);
	#(CPERIOD) itf.serial_data_rx = 1'b0;

	#(CPERIOD) itf.serial_data_rx = uart_in[0];
	#(CPERIOD) itf.serial_data_rx = uart_in[1];
	#(CPERIOD) itf.serial_data_rx = uart_in[2];
	#(CPERIOD) itf.serial_data_rx = uart_in[3];

	#(CPERIOD) itf.serial_data_rx = uart_in[4];
	#(CPERIOD) itf.serial_data_rx = uart_in[5];
	#(CPERIOD) itf.serial_data_rx = uart_in[6];
	#(CPERIOD) itf.serial_data_rx = uart_in[7];

    #(CPERIOD) itf.serial_data_rx = uart_in[0]^uart_in[1]^uart_in[2]^uart_in[3]^uart_in[4]^uart_in[5]^uart_in[6]^uart_in[7];//parry bit


	#(CPERIOD) itf.serial_data_rx = 1'b1;

delay_clk(10);


endtask



task automatic send_full_routine();
    send_rx_config(8'h03,8'h02);
    delay_clk(50); 
    send_rx_rtx_capture_procces(8'h02,RTX);
    delay_clk(50); 
    send_rx_rtx_capture_procces(8'h02,CAPTURE);
    delay_clk(50);
    send_rx_load(8'h0A,8'h00,8'h01, 'd6, 'h05_04_03_02_01_00); 
    delay_clk(50);
    send_rx_load(8'h0E,8'h01,8'h00, 'd10,'h0F_0E_0D_0C_0B_0A_09_08_07_06);
    delay_clk(100);
    send_rx_rtx_capture_procces(8'h02,8'h05);
    delay_clk(100);
endtask

task automatic init();
    itf.parallel_data_tx='0;
    itf.transmit = '0;
    itf.serial_data_rx = '0;
    itf.clr_interrupt = '0;
endtask


//Byte to send with UART
task automatic data_send_tx(byte_t	uart_in = 8'h01);
	#(CPERIOD) itf.parallel_data_tx = '0;

	#(CPERIOD) itf.parallel_data_tx = uart_in;

	   itf.transmit = 1;
	#(CPERIOD) itf.transmit = 0;
	delay_clk(50);
endtask

task automatic delay_clk(int dly = 1);
    repeat(dly)
        @(posedge itf.cclk);
endtask
`endprotect
endclass
