interface tb_qsys_if(
    input cclk 
);

    import tb_qsys_pkg::*;


    byte data;
    logic transmit;
    logic serial_data_rx;
    logic clr_interrupt;
    byte_t parallel_data_tx;

    modport tstr(
        input cclk,
        output transmit,
        output serial_data_rx,
        output clr_interrupt,
        output parallel_data_tx
    );
    modport top(
        input cclk,
        input transmit,
        input serial_data_rx,
        input clr_interrupt,
        input parallel_data_tx
    );

endinterface
