import wcrra_pkg::*;

module binary_decoding #(
    parameter DW = 4
)
(
    input data_t ppc_output,
    output dwb2log_data_t binary_decoded_index
);
dwb2log_data_t index;
logic found;
integer i;

always_comb begin
    found = 0;
    index = 0;
     for( i =0;i<DW; i++) begin
        if (ppc_output[i] == TRUE && found == FALSE) begin
            found = TRUE;
            index = i;
        end

 end 

end

assign binary_decoded_index  = index;
endmodule