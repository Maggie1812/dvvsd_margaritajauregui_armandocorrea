
module or_mod (
    input logic in_a,
    input logic in_b,
    output logic  out
);

assign out = in_a || in_b;
endmodule