`timescale 1ns / 1ps
// Engineer:        DSc Abisai Ramirez Perez 
// 
// Create Date:     June 6th, 2019
// Design Name: 
// Module Name:     fsm_dbcr
// Project Name:    debouncer
// Target Devices:  DE2-115
// Description:     This is the FSM of the debouncer  
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`ifndef FSM_DBCR_SV
    `define FSM_DBCR_SV

import dbcr_pkg::*;
import wcrra_pkg::*;
module fsm_dbcr 
(
    // Be aware that the clk could be 50 MHz or the output of your PLL
    input           clk,
    input           rst_n,
    input  logic    Din,
    input           Delay30ms_ready,
    output logic    EnableCounter,
    output logic    one_shot
    );


// States declaration
fsm_dbcr_state_e    current_state;


// Definition of the next state
always_ff@(posedge clk, negedge rst_n) begin
    if(!rst_n)
        current_state   <= BAJO;
    else begin
            case(current_state)
                BAJO:   if (Din ==  TRUE)
                            current_state   <= DLY1; // Defining the next state
                        else
                            current_state   <= BAJO;
                DLY1:  if (Delay30ms_ready)
                            current_state   <= ALTO; // Defining the next state
                        else
                            current_state   <= DLY1;
                ALTO:  if (Din == FALSE)
                            current_state   <= DLY2; // Defining the next state
                        else
                            current_state   <= ALTO;
                DLY2:   if (Delay30ms_ready)
                            current_state   <= BAJO; // Defining the next state
                        else
                            current_state   <= DLY2;
    		    endcase
    end
end


// Definition of the output (Combinational Output)
always_comb
    case (current_state)
        BAJO: begin
                EnableCounter   = FALSE;
                one_shot             = FALSE;
            end
        DLY1:    begin
                EnableCounter   = TRUE;
                one_shot             = Delay30ms_ready;  // This is a enable of a single tick clock
            end
        ALTO:    begin
                EnableCounter   = FALSE;
                one_shot             = FALSE;
            end
        DLY2: begin
                EnableCounter   = TRUE;
                one_shot             = FALSE;
            end
    endcase

endmodule
`endif
