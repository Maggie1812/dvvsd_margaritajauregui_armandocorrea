
`ifndef WCRRA_PKG_SV
    `define WCRRA_PKG_SV
package wcrra_pkg;
    localparam DW = 5;
    localparam DW_B2LOG = $clog2(DW);
    localparam bit TRUE     =1'b1;
    localparam bit FALSE    =1'b0;
    typedef logic [DW-1:0] data_t;
    typedef logic [DW_B2LOG-1:0] dwb2log_data_t;

endpackage
`endif 