`ifndef TB_PKG
	`define TB_PKG
package tb_rr_pkg;
localparam N_REQ = 4;
localparam WN_REQ = $clog2(N_REQ);
localparam PERIOD = 8;

typedef logic [N_REQ-1:0] rqst_t;
typedef logic [WN_REQ-1:0] grant_t;
endpackage
`endif
