
module rr_wrapper
    import wcrra_pkg::*;    
    import tb_rr_pkg::*;
    (
        input clk,
        input rst,
        tb_rr_if.rr itf
    );

    dwb2log_data_t grant;
wcrra #(
        .DW(DW)
)dut
(
    .cclk(itf.clk),
    .arst_n(rst),
    .requests(data_t'(itf.requests)),
    .ack(itf.ack),
    .valid(itf.valid),
    .grant(grant)
);

assign itf.grant = grant_t'(grant);
