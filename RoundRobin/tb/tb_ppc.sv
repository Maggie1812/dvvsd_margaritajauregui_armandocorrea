`timescale 1ns/ 1ps

module tb_ppc();
import wcrra_pkg::*;
localparam PERIOD = 8;
//localparam DataW = 5;
//Signal declaration
dwb2log_data_t grant;
bit clk;
bit rst_n;
logic ack;
logic valid;
data_t request_vector;
//Unit Under Test instance
 wcrra #(
    .DW(DW)
) uut
(
    .clk(clk),
    .rst_n(rst_n),
    .request_vector(request_vector),
    .ack(ack),
    .valid(valid),
    .grant(grant)
);



initial begin
    clk = 1'b0;
    rst_n = 1'b0;
    ack = 1'b0;
    request_vector = 5'b11010;
    #(PERIOD) rst_n = 1'b1;
    #(PERIOD) ack = 1'b1;
    #(PERIOD) ack = 1'b0;
    #(PERIOD) ack = 1'b1;
    #(PERIOD) ack = 1'b0;
    #(PERIOD) ack = 1'b1;
    #(PERIOD) ack = 1'b0;


    
end

always begin
    #(PERIOD/2) clk <= ~clk;
end

endmodule
