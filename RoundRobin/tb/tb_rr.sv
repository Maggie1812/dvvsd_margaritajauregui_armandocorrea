`timescale 1ns / 1ps
include "tester_rr.svh";
module tb_rr ();

import tb_rr_pkg::*;
integer text_id;
logic clk;
logic rst;

// Definition of tester
tester_rr  t     ;

// Instance of interface
tb_rr_if   itf(
.clk(clk)
) ;

rr_wrapper dut(
.clk(clk),
.rst(rst),
.itf (itf.rr)
);

initial begin
                t   = new(itf);
               t.init();
                clk = 'd0; 
                rst = 'd1; 
  #(2*PERIOD)   
  		rst = 'd0; 
  #(2*PERIOD)   rst = 'd1; 
                fork
                    t.open_file();
                    t.inject_all_rqst();
                    //t.review_output();
                join                
                t.close_file();
                $stop();
end
always begin
    #(PERIOD/2) clk =!clk;
end
endmodule
endmodule
