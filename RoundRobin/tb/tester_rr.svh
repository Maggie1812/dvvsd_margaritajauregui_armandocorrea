//Coder: Armando Emmanuel Correa Amorelli
//Date: 2021, 22th April


class tester_rr;



`ifdef INVERTED_PULSES
    localparam FALSE = 1'b1;
    localparam TRUE  = 1'b0;
`else
    localparam TRUE = 1'b1;
    localparam FALSE  = 1'b0;
`endif

`protect
localparam N_REQ = 4;
localparam W_N_REQ = $clog2(N_REQ);
typedef logic [N_REQ-1:0] rqst_t;
typedef logic [W_N_REQ-1:0] grant_t;

localparam PERIOD   = 4;
localparam DLY      = 2;
localparam INC      = 1;
localparam RQST_INIT = 'b0;
localparam RQST_LAST = {N_REQ{'b1}};
localparam ACK_LAST = {W_N_REQ{'b1}};
`endprotect

integer text_id;
grant_t ack_ctr;
rqst_t rqst_vect;
rqst_t q_rqst[$];
grant_t q_grnt[$];

virtual tb_rr_if itf;

`protect
function new(virtual tb_rr_if.tstr t);
    itf = t;
endfunction

task automatic inject_all_rqst();
    for(rqst_vect = RQST_INIT; rqst_vect < RQST_LAST; rqst_vect = rqst_vect+INC) begin
        q_rqst.push_front(rqst_vect);
        for(ack_ctr = 0; ack_ctr < ACK_LAST; ack_ctr = ack_ctr+INC) begin
            itf.ack = TRUE;
            #(PERIOD)itf.ack = FALSE;
            q_grnt.push_front(itf.grant);
        end
    end
endtask
task automatic init();
    itf.ack = FALSE;
    itf.requests = 'b0;
endtask

task grnt_calc(rqst_t rqst);

endtask

task automatic open_file();
    text_id = $fopen("report.txt", "w");
    //$display("File open %0d", text_id);
endtask

task automatic close_file();
    $fclose(text_id);
endtask
`endprotect
endclass
