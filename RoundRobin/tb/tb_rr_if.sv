interface tb_mdr_if(
    input bit clk
);
import tb_rr_pkg::*;
rqst_t   requests;
logic                 ack;
logic                 valid;
grant_t grant;

modport tstr(
    input clk,
    output requests,
    output ack,
    input valid,
    input grant
);

modport rr(
    input clk,
    input requests,
    input ack,
    output valid,
    output grant
);

endinterface
