onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_ppc/uut/ack_dbcr/clk
add wave -noupdate /tb_ppc/uut/ack_dbcr/rst_n
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_fsm_dbcr/current_state
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_cntr_mod_n/MAXCNT
add wave -noupdate /tb_ppc/uut/rst_dbcr/i_cntr_mod_n/FREQ
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_fsm_dbcr/Delay30ms_ready
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_cntr_mod_n/count
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_cntr_mod_n/ovf
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_cntr_mod_n/rst
add wave -noupdate /tb_ppc/uut/ack_dbcr/i_cntr_mod_n/enb
add wave -noupdate /tb_ppc/uut/ack_dbcr/Din
add wave -noupdate /tb_ppc/uut/ack_dbcr/one_shot
add wave -noupdate /tb_ppc/uut/clk
add wave -noupdate /tb_ppc/uut/rst_n
add wave -noupdate /tb_ppc/uut/request_vector
add wave -noupdate /tb_ppc/uut/ack
add wave -noupdate /tb_ppc/uut/valid
add wave -noupdate /tb_ppc/uut/grant
add wave -noupdate -divider UUT
add wave -noupdate /tb_ppc/request_vector
add wave -noupdate /tb_ppc/ack
add wave -noupdate /tb_ppc/grant
add wave -noupdate /tb_ppc/rst_n
add wave -noupdate -divider {Mask Logic}
add wave -noupdate -label {Request Vector} /tb_ppc/uut/request_vector
add wave -noupdate -label {Mask Vector} /tb_ppc/uut/mask_logic_unit/mask_vector
add wave -noupdate -label {Masked Request Vector} /tb_ppc/uut/mask_logic_unit/masked_request_vector
add wave -noupdate -divider {PPC INPUT MUX}
add wave -noupdate /tb_ppc/uut/ppc_input_mux/in_a
add wave -noupdate /tb_ppc/uut/ppc_input_mux/in_b
add wave -noupdate /tb_ppc/uut/ppc_input_mux/sel
add wave -noupdate /tb_ppc/uut/ppc_input_mux/out
add wave -noupdate -divider PPC
add wave -noupdate /tb_ppc/uut/ppc_unit/request_vector
add wave -noupdate /tb_ppc/uut/ppc_unit/ppc_output
add wave -noupdate -divider {PPC MUX}
add wave -noupdate /tb_ppc/uut/mask_vector_mux/in_a
add wave -noupdate /tb_ppc/uut/mask_vector_mux/in_b
add wave -noupdate /tb_ppc/uut/mask_vector_mux/sel
add wave -noupdate /tb_ppc/uut/mask_vector_mux/out
add wave -noupdate -divider PIPO
add wave -noupdate /tb_ppc/rst_n
add wave -noupdate /tb_ppc/uut/mask_next_pipo/en
add wave -noupdate /tb_ppc/uut/mask_next_pipo/dataIn
add wave -noupdate /tb_ppc/uut/mask_next_pipo/dataOut
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {18949 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 256
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {167242 ps}
